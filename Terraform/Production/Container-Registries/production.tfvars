resource_group_name           = "alex-test_rg_f2f66245-fe81-a27d-422a-5e698697188d"
container_reg_name_prefix     = "jihun"
container_reg_location        = "koreacentral"
container_reg_service_tier    = "Standard"
container_reg_tags            = {
    "envrionment"   = "dev"
    "monitoring"    = "true"
}
container_reg_geo_replication = ["eastus", "koreacentral"]