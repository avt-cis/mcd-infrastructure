variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type        = string
}

variable "container_reg_name_prefix" {
  description = "(required) Name Prefix of the Container Registries to deploy / (possible) 3 ~ 24 characters, lowercase letters, numbers"
  type        = string
}

variable "container_reg_location" {
  description = "(required) Location of the target Container Registries"
  type        = string
}

variable "container_reg_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

variable "container_reg_service_tier" {
  description = "(optional) Service Tier to deploy"
  type        = string
  validation {
    condition     = contains(["Basic", "Standard", "Premium"], var.container_reg_service_tier)
    error_message = "Possible values are 'Basic' or 'Standard', or 'Premium'."
  }
  default = "Standard"
}

variable "container_reg_geo_replication" {
  description = "(optional) Set of geo-replication used for this Container Registries, It is only available if the Service Tier is Premium"
  type        = list(string)
  default     = []
}