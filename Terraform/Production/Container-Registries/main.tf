resource "azurerm_container_registry" "acr" {
  name                = local.container_reg_name
  resource_group_name = var.resource_group_name
  location            = var.container_reg_location
  sku                 = var.container_reg_service_tier
  admin_enabled       = true
  tags                = var.container_reg_tags

    dynamic "georeplications" {
    for_each = var.container_reg_service_tier != "Premium" ? [] : var.container_reg_geo_replication
    content {
      location                = georeplications.value
      zone_redundancy_enabled = false
    }
  }
}