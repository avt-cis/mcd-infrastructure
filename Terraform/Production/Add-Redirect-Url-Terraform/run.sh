set -e
set -x

export ARM_SUBSCRIPTION_ID="9a8e1a59-ab84-49af-a717-7f2582116098"
#export ARM_CLIENT_ID="d107e0b1-ff40-4628-992a-4b9b1662851c"
#export ARM_CLIENT_SECRET="mcs7Q~KntigfARk121chbLvTkv57hDymUG4M5"
#export ARM_TENANT_ID="c12007a4-882b-4381-b05a-b783431570c7"

#az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID
az login -u "alex.choi@cognex.com" -p "@#CLOUDdeveloper32"
az account set --subscription $ARM_SUBSCRIPTION_ID

### import variable
export TF_VAR_object_id="5cb0f965-38d9-4045-abb9-99ed4551360d"

### delete '.tfstate' file
count=`find . -name "*.tfstate*" -type f  | wc -l`
if [ $count != 0 ]
then 
    find . -name "*.tfstate*" -type f -delete
fi

### operate terraform script
terraform init
terraform validate
terraform import azuread_application.ad $TF_VAR_object_id
terraform plan
terraform apply -auto-approve
terraform output -json > output.json