data "azuread_application" "ad_app"  {
    object_id = var.object_id
}

output "redirect_urls" {
    value = data.azuread_application.ad_app.single_page_application
}

resource "azuread_application" "ad" {
    device_only_auth_enabled       = false
    display_name                   = data.azuread_application.ad_app.display_name
    fallback_public_client_enabled = false
    group_membership_claims        = []
    identifier_uris                = []
    oauth2_post_response_required  = false
    owners                         = [
        "66cfaacf-a878-4459-82d3-414d469da9ec",
        "d886a140-9327-468b-b073-979370ab285f"
    ]
    prevent_duplicate_names        = false
    sign_in_audience               = "AzureADMyOrg"

    api {
        known_client_applications      = []
        mapped_claims_enabled          = false
        requested_access_token_version = 1
    }

    feature_tags {
        custom_single_sign_on = false
        enterprise            = false
        gallery               = false
        hide                  = false
    }

    public_client {
        redirect_uris = []
    }

    required_resource_access {
        resource_app_id = "00000003-0000-0000-c000-000000000000"

        resource_access {
            id   = "e1fe6dd8-ba31-4d61-89e7-88639da4683d"
            type = "Scope"
        }
        resource_access {
            id   = "a154be20-db9c-4678-8ab7-66f6cc099a59"
            type = "Scope"
        }
        resource_access {
            id   = "06da0dbc-49e2-44d2-8312-53f166ab848a"
            type = "Scope"
        }
        resource_access {
            id   = "c5366453-9fb0-48a5-a156-24f0c49a4b84"
            type = "Scope"
        }
        resource_access {
            id   = "0e263e50-5827-48a4-b97c-d940288653c7"
            type = "Scope"
        }
    }

    timeouts {}

    web {
        redirect_uris = var.platform_type ==    "web" ? var.redirect_uris : []
        implicit_grant {
            access_token_issuance_enabled = false
            id_token_issuance_enabled     = false
        }
    }

    single_page_application {
        redirect_uris = var.platform_type == "spa" ? var.redirect_uris : []
    }
}