# Configure Azure Services to Create
variable "object_id" {
  description = "(required) The object id of Azure Active Directory's App"
  type        = string
  default     = "1f865835-ce44-454d-859f-c9b06f75ab3c"
}

variable "platform_type" {
  description = "(required) The plaform type of web applications: 'web' or 'spa' for single page application"
  type        = string
  validation {
    condition     = contains(["web", "spa"], var.platform_type)
    error_message = "Argument 'platform_type' must be either of 'web', 'spa'."
  }
  default = "spa"
}

variable "redirect_uris" {
  description = "(required) List of redirect URIs to add to app registration"
  type        = list(string)
  default     = [
    "https://cideploymenttest.z1.web.core.windows.net/",
    "https://cloudtrainingdemo.z13.web.core.windows.net/",
    "http://localhost:4200/",
    "https://modulardlfrontendstatic.z22.web.core.windows.net/"
  ]
}