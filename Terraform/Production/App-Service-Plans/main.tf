data "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
}

resource "azurerm_app_service_plan" "asp" {
  name                = local.app_service_plan_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  kind                = var.app_service_plan_kind
  reserved            = true

  sku {
    tier = var.app_service_plan_sku.tier
    size = var.app_service_plan_sku.size
  }
} 