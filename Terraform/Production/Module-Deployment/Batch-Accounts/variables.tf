variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type        = string
}

variable "batch_acc_name_prefix" {
  description = "(required) Name Prefix of the Batch Accounts to deploy / (possible) 3 ~ 24 characters, lowercase letters, numbers"
  type        = string
}

variable "batch_acc_location" {
  description = "(required) location of the Batch Accounts"
  type        = string
}

variable "batch_acc_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

variable "batch_pool" {
  description = "(optional) Batch Account Pool configurations"
  type        = map(object({
    pool_name = string
    pool_vm_size = string
    pool_node_agent_sku_id = string
    pool_operating_system_publisher = string
    pool_operating_system_offer = string
    pool_operating_system_sku = string
    pool_scale_evaluation_interval = string
    pool_scale_formula = string
  }))
  default     = {}
}