resource "azurerm_batch_account" "batch_acc" {
  resource_group_name             = var.resource_group_name
  location                        = var.batch_acc_location
  name                            = local.batch_acc_name
  pool_allocation_mode            = "BatchService"
  public_network_access_enabled   = true
  tags                            = var.batch_acc_tags
}

resource "azurerm_batch_pool" "batch_pool" {
  for_each = var.batch_pool

  name                = each.value.pool_name
  resource_group_name = var.resource_group_name
  account_name        = azurerm_batch_account.batch_acc.name
  vm_size             = each.value.pool_vm_size
  node_agent_sku_id   = each.value.pool_node_agent_sku_id

  storage_image_reference {
    publisher = each.value.pool_operating_system_publisher
    offer     = each.value.pool_operating_system_offer
    sku       = each.value.pool_operating_system_sku
    version   = "latest"
  }

  auto_scale {
      evaluation_interval = each.value.pool_scale_evaluation_interval
      formula             = each.value.pool_scale_formula
  }

  container_configuration {
      type = "DockerCompatible"
  }
}