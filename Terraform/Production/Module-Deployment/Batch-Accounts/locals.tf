resource "random_string" "random" {
  length           = 8
  special          = false
  number           = true
}

locals {
  batch_acc_name = format(
    "%s%s",
    lower(var.batch_acc_name_prefix),
    lower(random_string.random.result)
  )
}