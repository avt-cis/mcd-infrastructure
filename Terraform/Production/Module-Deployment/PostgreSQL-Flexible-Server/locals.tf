resource "random_string" "random" {
  length  = 16
  special = false
}

resource "random_id" "server" {
  byte_length = 16
}

locals {
  psql_flex_server_name = format(
      "%s-psql-%s",
      lower(var.psql_flex_server_name_prefix),
      lower(random_string.random.result)
  )

  psql_admin_password = format(
      "psql-%s",
      random_id.server.id
  )
}