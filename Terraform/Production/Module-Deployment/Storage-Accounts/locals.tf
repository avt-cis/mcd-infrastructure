locals {
  storage_acc_name = substr(replace(lower(var.storage_acc_name_prefix), "-", ""), 0, 24)
}