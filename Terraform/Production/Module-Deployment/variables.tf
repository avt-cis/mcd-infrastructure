###########################################################
#  Common Variables
###########################################################
variable "location" {
  description = "(required) Region to create the resource"
  type        = string
  default     = "eastus"
}

###########################################################
#  Resource Groups Variables
###########################################################
variable "resource_group_name_prefix" {
  description = "(required) Prefix of resource group name"
  type        = string
}

variable "resource_group_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

###########################################################
#  Storage Accounts Variables
###########################################################
variable "storage_acc_name_prefix" {
  description = "(required) Prefix of storage account name"
  type        = string
}

variable "storage_acc_account_tier" {
  description = "(optional) Account Tier of storage account to deploy"
  type        = string
  default     = "Standard"
}

variable "storage_acc_tags" {
  description = "(optional) Some usefule information for storage account"
  type        = map(any)
  default     = {}
}

variable "storage_acc_replication_type" {
  description = "(optional) Replication type of storage account to deploy"
  type        = string
  default     = "LRS"
}

###########################################################
#  PostgreSQL Flexible Server Variables
###########################################################
variable "psql_admin_name" {
  description = "(required) Primary administrator username for the server / (possible) 1 ~ 63 characters and numbers"
  type        = string
}

variable "psql_flex_server_name_prefix" {
  description = "(required) Name Prefix of the Azure Database for PostgreSQL flexible servers to deploy / (possible) 3 ~ 63 characters, lowercase letters, numbers, hyphens"
  type        = string
}

variable "psql_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

variable "psql_compute_tier_size" {
  description = "(required) The computer tier and size for the server. the name follow the tier + name pattern. (e.g. B_Standard_B1ms, GP_Standard_D2s_v3, MO_Standard_E4s_v3)."
  type        = string
}

variable "psql_storage_size" {
  description = "(required) The max storage(in MB) allowed for the server."
  type        = number
}

variable "psql_availability_zone" {
  description = "(required) The availability zone of the server."
  type        = number
}


variable "development_mode" {
  description = "(optional) If 'development_mode' is true, All IP addresses is allowed access to server"
  type        = bool
  default     = false
}

variable "allow_all_ip" {
  description = "(optional) Configuration for allowing access to all IPs for server."
  type        = object({
    name              = string
    start_ip_address  = string
    end_ip_address    = string
  })
  default = {
    name              = "allow_all_ip"
    start_ip_address  = "0.0.0.0"
    end_ip_address    = "255.255.255.255"
  }
}

###########################################################
#  Batch Accounts Variables
###########################################################
variable "batch_acc_name_prefix" {
  description = "(required) Name Prefix of the Batch Accounts to deploy / (possible) 3 ~ 24 characters, only allows lowercase letters, numbers"
  type        = string
}

variable "batch_pool" {
  description = "(optional) Batch Pool configurations"
  type        = map(object({
    pool_name = string
    pool_vm_size = string
    pool_node_agent_sku_id = string
    pool_operating_system_publisher = string
    pool_operating_system_offer = string
    pool_operating_system_sku = string
    pool_scale_evaluation_interval = string
    pool_scale_formula = string
  }))
  default     = {}
}

variable "batch_acc_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

###########################################################
#  App Services Variables
###########################################################
variable "app_service_name_prefix" {
  description = "(required) Name prefix of the App Service to deploy"
  type        = string
}

###########################################################
#  Container Registries Variables
###########################################################
variable "container_reg_name_prefix" {
  description = "(required) Name Prefix of the Container Registries to deploy / (possible) 3 ~ 24 characters, lowercase letters, numbers"
  type        = string
}

variable "container_reg_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

variable "container_reg_service_tier" {
  description = "(optional) Service Tier to deploy"
  type        = string
  validation {
    condition     = contains(["Basic", "Standard", "Premium"], var.container_reg_service_tier)
    error_message = "Possible values are 'Basic' or 'Standard', or 'Premium'."
  }
}

variable "container_reg_geo_replication" {
  description = "(optional) A set of geo-replication used for this Container Registries"
  type        = list(string)
  default     = []
}

###########################################################
#  Function Apps Variables
###########################################################
variable "function_app_names" {
  description = "(required) List of names of the Function App to deploy"
  type        = list
}

variable "function_app_storage_acc_replication_type" {
  description = "(optional) Storage account replication type for Function App to deploy"
  type        = string
  default     = "LRS"
}

variable "function_app_db_connection_string" {
  description = "(required) Name of the database connection for Function App to deploy"
  type        = string
}

variable "function_app_worker_runtime" {
  description = "(required) Type of worker runtime for the Function App to deploy"
  type        = string
  default     = "python"
}