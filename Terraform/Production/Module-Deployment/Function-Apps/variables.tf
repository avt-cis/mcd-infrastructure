variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type = string
}

variable "resource_location" {
  description = "(required) Location of the target resource to deploy"
  type = string
}

variable "function_app_name" {
  description = "(required) Name of the Function App to deploy"
  type        = string
}

variable "function_app_storage_acc_replication_type" {
  description = "(optional) Storage account replication type for Function App to deploy"
  type        = string
  default     = "LRS"
}

variable "function_app_db_connection_string" {
  description = "(required) Name of the database connection for Function App to deploy"
  type        = string
}

variable "function_app_worker_runtime" {
  description = "(required) Type of worker runtime for the Function App to deploy"
  type        = string
  default     = "python"
}
