module "storage_accounts" {
  source = "../Storage-Accounts"

  resource_group_name          = var.resource_group_name
  storage_acc_name_prefix      = var.function_app_name
  storage_acc_location          = var.resource_location
  storage_acc_account_tier      = "Standard"
  storage_acc_replication_type  = var.function_app_storage_acc_replication_type
  storage_acc_tags              = {}
}

module "app_service_plans" {
  source = "../App-Service-Plans"

  resource_group_name       = var.resource_group_name
  app_service_plan_location = var.resource_location
  app_service_name          = local.app_service_plan_name
  app_service_plan_kind     = "Linux"
  app_service_plan_sku      = {
    tier = "Dynamic"
    size = "Y1"
  }
}

data "azurerm_storage_account" "storageAcc" {
  name                = module.storage_accounts.storage_acc_name
  resource_group_name = var.resource_group_name
}

resource "azurerm_function_app" "functionapp" {
  name                            = var.function_app_name
  location                        = var.resource_location
  resource_group_name             = var.resource_group_name
  app_service_plan_id             = module.app_service_plans.app_service_plan_id
  storage_account_name            = module.storage_accounts.storage_acc_name
  storage_account_access_key      = data.azurerm_storage_account.storageAcc.primary_access_key
  os_type                         = "linux"
  version                         = "~4"

  app_settings                    = {
      "DatabaseConnectionString"        = var.function_app_db_connection_string
      "FUNCTIONS_WORKER_RUNTIME"        = var.function_app_worker_runtime
      "APPINSIGHTS_INSTRUMENTATIONKEY"  = ""
  }
  daily_memory_time_quota         = 0
  enable_builtin_logging          = false
  enabled                         = true
  https_only                      = false
  tags                            = {}

  auth_settings {
      additional_login_params        = {}
      allowed_external_redirect_urls = []
      enabled                        = false
      token_refresh_extension_hours  = 0
      token_store_enabled            = false
  }

  site_config {
      always_on                        = false
      app_scale_limit                  = 200
      dotnet_framework_version         = "v4.0"
      elastic_instance_minimum         = 0
      ftps_state                       = "AllAllowed"
      http2_enabled                    = false
      ip_restriction                   = []
      min_tls_version                  = "1.2"
      pre_warmed_instance_count        = 0
      runtime_scale_monitoring_enabled = false
      scm_ip_restriction               = []
      scm_type                         = "None"
      scm_use_main_ip_restriction      = false
      use_32_bit_worker_process        = false
      vnet_route_all_enabled           = false
      websockets_enabled               = false

      cors {
          allowed_origins     = [
              "https://portal.azure.com",
          ]
          support_credentials = false
      }
  }

  timeouts {}
}
