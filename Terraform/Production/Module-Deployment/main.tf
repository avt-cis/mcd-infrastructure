###########################################################
#  Resource Groups Deployment
###########################################################
module "resource_group" {
  source = "./Resource-Groups"

  resource_group_name_prefix  = var.resource_group_name_prefix
  resource_group_location     = var.location
  resource_group_tags         = var.resource_group_tags
}

###########################################################
#  Storage Accounts Deployment
###########################################################
module "storage_accounts" {
  source = "./Storage-Accounts"

  resource_group_name           = module.resource_group.resource_group_name
  storage_acc_name_prefix       = var.storage_acc_name_prefix
  storage_acc_location          = module.resource_group.resource_group_location
  storage_acc_account_tier      = var.storage_acc_account_tier
  storage_acc_replication_type  = var.storage_acc_replication_type
  storage_acc_tags              = var.storage_acc_tags
}

###########################################################
#  PostgreSQL Flexible Server Deployment
###########################################################
module "postgresql_flexible_server" {
  source = "./PostgreSQL-Flexible-Server"

  resource_group_name           = module.resource_group.resource_group_name
  psql_server_location          = module.resource_group.resource_group_location
  psql_admin_name               = var.psql_admin_name
  psql_flex_server_name_prefix  = var.psql_flex_server_name_prefix
  psql_compute_tier_size        = var.psql_compute_tier_size
  psql_storage_size             = var.psql_storage_size
  psql_availability_zone        = var.psql_availability_zone
  psql_tags                     = var.psql_tags
}

###########################################################
#  Batch Accounts Deployment
###########################################################
module "batch_accounts" {
  source = "./Batch-Accounts"

  resource_group_name       = module.resource_group.resource_group_name
  batch_acc_name_prefix     = var.batch_acc_name_prefix
  batch_acc_location        = module.resource_group.resource_group_location
  batch_acc_tags            = var.batch_acc_tags
  batch_pool                = var.batch_pool
}

###########################################################
#  Frontend App Service Deployment
###########################################################
module "frontend_app_service" {
  source = "./App-Services"

  resource_group_name           = module.resource_group.resource_group_name
  app_service_plan_location     = module.resource_group.resource_group_location
  app_service_name_prefix       = var.app_service_name_prefix
}

###########################################################
#  Container Registries Deployment
###########################################################
module "container_registries" {
  source = "./Container-Registries"

  resource_group_name           = module.resource_group.resource_group_name
  container_reg_location        = module.resource_group.resource_group_location
  container_reg_name_prefix     = var.container_reg_name_prefix
  container_reg_service_tier    = var.container_reg_service_tier
  container_reg_tags            = var.container_reg_tags
  container_reg_geo_replication = var.container_reg_geo_replication
}

###########################################################
#  Function Apps Deployment
###########################################################
module "function_apps" {
  source = "./Function-Apps"

  resource_group_name                       = module.resource_group.resource_group_name
  resource_location                         = module.resource_group.resource_group_location
  function_app_name                         = "${var.function_app_names[count.index]}"
  function_app_storage_acc_replication_type = var.function_app_storage_acc_replication_type
  function_app_db_connection_string         = var.function_app_db_connection_string
  function_app_worker_runtime               = var.function_app_worker_runtime
  count                                     = "${length(var.function_app_names)}"
}