resource "random_string" "random" {
  length  = 8
  special = false
}

locals {
  container_reg_name = format(
    "%s%s",
    lower(var.container_reg_name_prefix),
    lower(resource.random_string.random.result)
  )
}