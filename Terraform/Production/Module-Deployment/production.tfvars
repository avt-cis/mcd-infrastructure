###########################################################
#  Common Configurations
###########################################################
location = "koreacentral"

###########################################################
#  Resource Groups Configurations
###########################################################
resource_group_name_prefix = "mdl_dev"
resource_group_tags = {
  "envrionment" = "dev"
  "monitoring"  = "true"
}

###########################################################
#  Storage Accounts Configurations
###########################################################
storage_acc_name_prefix  = "mdldev"
storage_acc_account_tier = "Standard"
storage_acc_replication_type = "RAGRS"
storage_acc_tags         = {
  "envrionment" = "dev"
  "monitoring"  = "true"
}

###########################################################
#  PostgreSQL Flexible Server Configurations
###########################################################
psql_admin_name               = "psqluser"
psql_flex_server_name_prefix  = "mdl-dev"
psql_compute_tier_size        = "B_Standard_B1ms"
psql_storage_size             = 32768
psql_availability_zone        = 2
psql_tags = {
  "envrionment" = "dev"
  "monitoring"  = "true"
}

###########################################################
#  Batch Accounts Configurations
###########################################################
batch_acc_name_prefix  = "mdldev"

batch_acc_tags = {
  "envrionment" = "dev"
  "monitoring"  = "true"
}

batch_pool = {
  "el_segmenter" = {
    "pool_name" = "el_segmenter"
    "pool_vm_size"  = "STANDARD_D2_V3"
    "pool_node_agent_sku_id"  = "batch.node.ubuntu 20.04"
    "pool_operating_system_publisher" = "microsoft-azure-batch"
    "pool_operating_system_offer" = "ubuntu-server-container"
    "pool_operating_system_sku" = "20-04-lts"
    "pool_scale_evaluation_interval" = "PT5M"
    "pool_scale_formula" = "// starting number of VMs\n        totalDedicatedNodes=1;\n        // In this formula, the pool size is adjusted based on the number of tasks in the queue. \n        // Note that both comments and line breaks are acceptable in formula strings.\n\n        // Get pending tasks for the past 3 minutes.\n        // get sample every 30 seconds --> 180 seconds means 6 times\n        samples = $ActiveTasks.GetSamplePercent(TimeInterval_Minute * 3); \n\n        // If we have fewer than 70 percent data points, we use the last sample point, otherwise we use the maximum of last sample point and the history average.\n        // if sample percentage is less than 50 --> means 3 tasks \n        tasks = samples < 70 ? max(0, avg($ActiveTasks.GetSample(1))) : max( $ActiveTasks.GetSample(1), avg($ActiveTasks.GetSample(TimeInterval_Minute * 3)));\n\n        // If number of pending tasks is not 0, set targetVM to pending tasks, otherwise set to 0, since there is usually long intervals between job submissions.\n        targetVMs = tasks > 0 ? tasks : 1;\n\n        // The max pool size is capped at 40, if target VM value is more than that, set it to 40.\n        cappedPoolSize = 20;\n        totalDedicatedNodes = max(1, min(targetVMs, cappedPoolSize));\n\n        lifespan         = time() - time(\"2021-12-06T08:26:24+00:00\");\n        span             = TimeInterval_Minute * 60;\n        startup          = TimeInterval_Minute * 10;\n        ratio            = 50;\n        // Check time of execution and check Tasks to reduce 0 \n        totalDedicatedNodes = (lifespan > startup ? (max($RunningTasks.GetSample(span, ratio), $ActiveTasks.GetSample(span, ratio)) == 0 ? 0 : totalDedicatedNodes) : totalDedicatedNodes);\n\n        $TargetDedicatedNodes = totalDedicatedNodes;\n\n        // Set node deallocation mode - keep nodes active only until tasks finish\n        $NodeDeallocationOption = taskcompletion;"
  }
  "vidi" = {
    "pool_name" = "vidi"
    "pool_vm_size"  = "STANDARD_NC4as_T4_V3"
    "pool_node_agent_sku_id"  = "batch.node.ubuntu 20.04"
    "pool_operating_system_publisher" = "microsoft-azure-batch"
    "pool_operating_system_offer" = "ubuntu-server-container"
    "pool_operating_system_sku" = "20-04-lts"
    "pool_scale_evaluation_interval" = "PT5M"
    "pool_scale_formula" = "// starting number of VMs\n        totalDedicatedNodes=1;\n        // In this formula, the pool size is adjusted based on the number of tasks in the queue. \n        // Note that both comments and line breaks are acceptable in formula strings.\n\n        // Get pending tasks for the past 3 minutes.\n        // get sample every 30 seconds --> 180 seconds means 6 times\n        samples = $ActiveTasks.GetSamplePercent(TimeInterval_Minute * 3); \n\n        // If we have fewer than 70 percent data points, we use the last sample point, otherwise we use the maximum of last sample point and the history average.\n        // if sample percentage is less than 50 --> means 3 tasks \n        tasks = samples < 70 ? max(0, avg($ActiveTasks.GetSample(1))) : max( $ActiveTasks.GetSample(1), avg($ActiveTasks.GetSample(TimeInterval_Minute * 3)));\n\n        // If number of pending tasks is not 0, set targetVM to pending tasks, otherwise set to 0, since there is usually long intervals between job submissions.\n        targetVMs = tasks > 0 ? tasks : 1;\n\n        // The max pool size is capped at 40, if target VM value is more than that, set it to 40.\n        cappedPoolSize = 20;\n        totalDedicatedNodes = max(1, min(targetVMs, cappedPoolSize));\n\n        lifespan         = time() - time(\"2021-12-06T08:26:24+00:00\");\n        span             = TimeInterval_Minute * 60;\n        startup          = TimeInterval_Minute * 10;\n        ratio            = 50;\n        // Check time of execution and check Tasks to reduce 0 \n        totalDedicatedNodes = (lifespan > startup ? (max($RunningTasks.GetSample(span, ratio), $ActiveTasks.GetSample(span, ratio)) == 0 ? 0 : totalDedicatedNodes) : totalDedicatedNodes);\n\n        $TargetDedicatedNodes = totalDedicatedNodes;\n\n        // Set node deallocation mode - keep nodes active only until tasks finish\n        $NodeDeallocationOption = taskcompletion;"
  }
}

###########################################################
#  App Services Configurations
###########################################################
app_service_name_prefix = "frontend"

###########################################################
#  Container Registries Configurations
###########################################################
container_reg_name_prefix     = "mdldev"
container_reg_service_tier    = "Standard"
container_reg_tags            = {
    "envrionment"   = "dev"
    "monitoring"    = "true"
}
container_reg_geo_replication = ["westus", "koreacentral"]

###########################################################
#  Function Apps Configurations
###########################################################
function_app_names = [
  "mdl-image-management-fct-dev",
  "mdl-labelling-fct-dev",
  "mdl-processing-backend-edgelearning-fct-dev",
  "mdl-processing-backend-fct-dev",
  "mdl-processing-backend-internal-fct-dev",
  "mdl-processing-backend-vidi-fct-dev",
  "mdl-project-management-fct-dev",
  "mdl-training-backend-edgelearning-fct-dev",
  "mdl-training-backend-fct-dev",
  "mdl-training-backend-internal-fct-dev",
  "mdl-training-backend-vidi-fct-dev",
  "mdl-validation-fct-dev",
  "mdl-validation-segmentation-fct-dev"
]
function_app_storage_acc_replication_type = "LRS"
function_app_db_connection_string = ""
function_app_worker_runtime = ""