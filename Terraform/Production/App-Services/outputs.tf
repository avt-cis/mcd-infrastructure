output "resource_group_name" {
  description = "The name of resource group created."
  value       = data.azurerm_resource_group.rg.name
}

output "app_service_plan_name" {
  description = "The name of app service plan deployed."
  value       = azurerm_app_service_plan.asp.name
}

output "app_service_plan_id" {
  description = "The ID of app service plan deployed."
  value       = azurerm_app_service.as.app_service_plan_id
}

output "app_service_plan_location" {
  description = "The location of app service plan deployed."
  value       = azurerm_app_service_plan.asp.location
}

output "app_service_plan_kind" {
  description = "The kind of app service plan deployed."
  value       = azurerm_app_service_plan.asp.kind
}

output "app_service_plan_sku" {
  description = "The sku of app service plan deployed."
  value       = azurerm_app_service_plan.asp.sku
}

output "app_service_name" {
  description = "The name of the app service deployed."
  value       = azurerm_app_service.as.name
}

output "app_service_location" {
  description = "The location of the app service deployed."
  value       = azurerm_app_service.as.location
}
