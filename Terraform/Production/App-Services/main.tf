module "app_service_plans" {
  source = "../App-Service-Plans"

  resource_group_name           = module.resource_group.resource_group_name
  app_service_plan_location     = module.resource_group.resource_group_location
  app_service_plan_name_prefix  = var.app_service_plan_name_prefix
  app_service_plan_kind         = var.app_service_plan_kind
  app_service_plan_sku          = var.app_service_plan_sku
}

/*
resource "azurerm_app_service_plan" "asp" {
  name                = local.app_service_plan_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  kind                = var.app_service_plan_kind
  reserved            = true

  sku {
    tier = var.app_service_plan_sku.tier
    size = var.app_service_plan_sku.size
  }
}

data "azurerm_app_service_plan" "asp" {
  name                = azurerm_app_service_plan.asp.name
  resource_group_name = data.azurerm_resource_group.rg.name
}

resource "azurerm_app_service" "as" {
  name                = data.azurerm_app_service_plan.asp.name
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
  app_service_plan_id = data.azurerm_app_service_plan.asp.id

  site_config {
    acr_use_managed_identity_credentials = false
    always_on                            = true
    default_documents = [
      "Default.htm",
      "Default.html",
      "Default.asp",
      "index.htm",
      "index.html",
      "iisstart.htm",
      "default.aspx",
      "index.php",
      "hostingstart.html",
    ]
    dotnet_framework_version    = "v4.0"
    ftps_state                  = "AllAllowed"
    http2_enabled               = false
    ip_restriction              = []
    local_mysql_enabled         = false
    managed_pipeline_mode       = "Integrated"
    min_tls_version             = "1.2"
    number_of_workers           = 1
    remote_debugging_enabled    = false
    scm_ip_restriction          = []
    scm_type                    = "None"
    scm_use_main_ip_restriction = false
    use_32_bit_worker_process   = true
    vnet_route_all_enabled      = false
    websockets_enabled          = false
  }

  timeouts {}
}
*/
