variable "app_service_plan_sku" {
  description = "(required) A sku block as documented below."
  type        = object({
    tier = string
    size = string
  })
  default = {
    tier = "Standard"
    size = "S1"
  }
}