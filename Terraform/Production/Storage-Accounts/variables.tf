variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type        = string
}

variable "storage_acc_name_prefix" {
  description = "(required) Name Prefix of the Storage Account to deploy"
  type        = string
}

variable "storage_acc_account_tier" {
  description = "(optional) Account Tier to deploy"
  type        = string
  validation {
    condition     = contains(["Standard", "Premium"], var.storage_acc_account_tier)
    error_message = "Argument 'account_tier' must either of 'Standard', or 'Premium'."
  }
  default = "Standard"
}

variable "storage_acc_tags" {
  description = "(optional) Additional information for resource management"
  type        = map(any)
  default     = {}
}