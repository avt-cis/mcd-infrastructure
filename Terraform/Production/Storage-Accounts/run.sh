set -e
set -x

az account set -s "bffb5be7-cc13-43b6-90d9-38964ec0b45b"

terraform init
terraform validate
terraform plan -var-file 'production.tfvars'
terraform apply -var-file 'production.tfvars' -auto-approve
terraform output -json > output.json
# terraform destroy -var-file 'production.tfvars' -auto-approve