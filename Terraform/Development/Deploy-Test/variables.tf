# Configure the Microsoft Azure Provider
variable "subscription_id" {
    description     = "(Required) The subscription ID"
    type            = string
}

variable "group_name" {
    description     = "The name of resource group"
    type            = string
}

variable "location" {
    description     = "The Azure Region in which all resources in this example should be created"
    type            = string
}