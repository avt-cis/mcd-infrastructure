subscription_id     = "9a8e1a59-ab84-49af-a717-7f2582116098" // Modular-DeepLearning-test
prefix              = "terraform"
group_name          = "terraform-test-rg"
location            = "eastus"
publisher_name      = "Alex Choi"
publisher_email     = "alex.choi@cognex.com"
sku_name            = "Consumption_0"
apim_policy         = <<XML
    <policies>
        <inbound>
            <validate-jwt header-name="Authorization" failed-validation-httpcode="401" failed-validation-error-message="Unauthorized. Access token is missing or invalid.">
                <openid-config url="https://login.microsoftonline.com/organizations/v2.0/.well-known/openid-configuration" />
                <issuers>
                    <issuer>https://sts.windows.net/organizations</issuer>
                </issuers>
            </validate-jwt>
            <cors allow-credentials="true">
                <allowed-origins>
                    <origin>https://modulardlfrontendstatic.z22.web.core.windows.net</origin>
                    <origin>http://localhost:4200</origin>
                </allowed-origins>
                <allowed-methods preflight-result-max-age="300">
                    <method>GET</method>
                    <method>POST</method>
                    <method>PATCH</method>
                    <method>PUT</method>
                    <method>DELETE</method>
                </allowed-methods>
                <allowed-headers>
                    <header>*</header>
                </allowed-headers>
                <expose-headers>
                    <header>*</header>
                </expose-headers>
            </cors>
        </inbound>
        <backend />
        <outbound />
        <on-error />
    </policies>
    XML
