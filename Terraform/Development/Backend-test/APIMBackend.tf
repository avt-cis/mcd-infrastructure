provider "azurerm" {
    features {}
}

resource "azurerm_api_management_backend" "example" {
  name                  = "example-backend"
  resource_group_name   = "alextestrg"
  api_management_name   = "modular-dl-apim-service"
  protocol              = "http"
  url                   = "https://mcd-project-management-fct.azurewebsites.net/api"

  credentials {
    header = {
      "x-functions-key" = "wVCkHZ2jaXZGjuQ2ufnN4ZX4sOFOhg6e4EH6D0j55GVNUHA6dHZ4Bw=="
    }
  }
}