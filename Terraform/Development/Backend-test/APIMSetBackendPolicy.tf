resource "azurerm_api_management_api_policy" "example" {
    api_name            = azurerm_api_management_api.example.name
    api_management_name = azurerm_api_management_api.example.api_management_name
    resource_group_name = azurerm_api_management_api.example.resource_group_name

    xml_content = <<XML
        <policies>
            <inbound>
            <base/>
            <set-backend-service id="apim-generated-policy" backend-id="mcd-project-management-fct" />
            </inbound>
        </policies>
    XML
}