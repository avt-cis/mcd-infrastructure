resource "azurerm_api_management_api" "example" {
    name                    = "example-api"
    resource_group_name     = "alextestrg"
    api_management_name     = "modular-dl-apim-service"
    revision                = "1"
    display_name            = "Example API"
    path                    = "example"
    protocols               = ["https"]

    import {
        content_format      = "openapi"
        content_value       = file("${path.module}/FuncOpenAPI3.yml")
    }
}