resource "azurerm_key_vault" "example" {
    access_policy                   = [
        {
            application_id          = ""
            certificate_permissions = [
                "all",
            ]
            key_permissions         = [
                "all",
            ]
            object_id               = "f5845714-ca80-44ee-aa51-4c13e2e40bc1"
            secret_permissions      = [
                "all",
            ]
            storage_permissions     = [
                "all",
            ]
            tenant_id               = "c12007a4-882b-4381-b05a-b783431570c7"
        },
        {
            application_id          = ""
            certificate_permissions = []
            key_permissions         = []
            object_id               = "b3e36fb2-f9dd-4497-b1bb-b8f8d1ef2f44"
            secret_permissions      = [
                "Get",
            ]
            storage_permissions     = []
            tenant_id               = "c12007a4-882b-4381-b05a-b783431570c7"
        },
    ]
    enable_rbac_authorization       = false
    enabled_for_deployment          = false
    enabled_for_disk_encryption     = false
    enabled_for_template_deployment = false
    id                              = "/subscriptions/9a8e1a59-ab84-49af-a717-7f2582116098/resourceGroups/akv-test-rg/providers/Microsoft.KeyVault/vaults/wdkeyvaulttest2"
    location                        = "koreacentral"
    name                            = "wdkeyvaulttest2"
    purge_protection_enabled        = false
    resource_group_name             = "akv-test-rg"
    sku_name                        = "standard"
    soft_delete_enabled             = true
    soft_delete_retention_days      = 90
    tags                            = {}
    tenant_id                       = "c12007a4-882b-4381-b05a-b783431570c7"
    vault_uri                       = "https://wdkeyvaulttest2.vault.azure.net/"

    network_acls {
        bypass                     = "AzureServices"
        default_action             = "Allow"
        ip_rules                   = []
        virtual_network_subnet_ids = []
    }

    timeouts {}
}