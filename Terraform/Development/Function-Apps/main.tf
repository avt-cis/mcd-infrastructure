resource "azurerm_storage_account" "example" {
  name                     = "alexsatestfk"
  resource_group_name      = var.resource_group_name
  location                 = var.resource_group_location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "example" {
  name                = "mdl-functions-test-service-plan"
  resource_group_name = var.resource_group_name
  location            = var.resource_group_location
  kind                = "Windows"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_function_app" "functionapp1" {
    name                            = "mdl-project-management-fct-dev"
    location                        = var.resource_group_location
    resource_group_name             = var.resource_group_name
    app_service_plan_id             = azurerm_app_service_plan.example.id
    storage_account_name            = azurerm_storage_account.example.name
    storage_account_access_key      = azurerm_storage_account.example.primary_access_key
    os_type                         = ""
    version                         = "~4"

    app_settings                    = {
        "DatabaseConnectionString"       = "postgres://mcd:Cognoid123@mcd-cgnx-postgresdb-prod.postgres.database.azure.com/projects?sslmode=require"
        "FUNCTIONS_WORKER_RUNTIME"       = "python"
    }
    daily_memory_time_quota         = 0
    enable_builtin_logging          = false
    enabled                         = true
    https_only                      = true
    
    tags                            = {}

    auth_settings {
        additional_login_params        = {}
        allowed_external_redirect_urls = []
        enabled                        = false
        token_refresh_extension_hours  = 0
        token_store_enabled            = false
    }

    site_config {
        always_on                        = false
        app_scale_limit                  = 200
        dotnet_framework_version         = "v4.0"
        elastic_instance_minimum         = 0
        ftps_state                       = "AllAllowed"
        http2_enabled                    = false
        ip_restriction                   = []
        min_tls_version                  = "1.2"
        pre_warmed_instance_count        = 0
        runtime_scale_monitoring_enabled = false
        scm_ip_restriction               = []
        scm_type                         = "None"
        scm_use_main_ip_restriction      = false
        use_32_bit_worker_process        = false
        vnet_route_all_enabled           = false
        websockets_enabled               = false

        cors {
            allowed_origins     = [
                "https://portal.azure.com",
            ]
            support_credentials = false
        }
    }

    timeouts {}
}