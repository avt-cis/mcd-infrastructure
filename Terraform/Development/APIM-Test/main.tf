/*
NOTE:
    For more information regading azurerm_api_management,
    please refer to https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/api_management
*/

provider "azurerm" {
    features {}

    subscription_id         = var.subscription_id
}

resource "azurerm_api_management_backend" "api" {
    name                = "api-backend"
    resource_group_name = var.group_name
    api_management_name = "${var.prefix}-apim-service"
    protocol            = "http"
    url                 = "https://mcd-project-management-fct.azurewebsites.net"
}

resource "azurerm_api_management" "apim_service" {
    name                    = "${var.prefix}-api"
    location                = var.location
    resource_group_name     = var.group_name
    publisher_name          = var.publisher_name
    publisher_email         = var.publisher_email
    sku_name                = var.sku_name
    tags = {
        Environment         = "Example"
    }
}

resource "azurerm_api_management_api" "api" {
    name                    = azurerm_api_management.apim_service.name
    resource_group_name     = azurerm_api_management.apim_service.resource_group_name
    api_management_name     = azurerm_api_management.apim_service.name
    revision                = "1"
    display_name            = "${var.prefix}-api"
    path                    = "example"
    protocols               = ["https", "http"]
    description             = "An example API"
    import {
        content_format      = var.open_api_spec_content_format
        content_value       = var.open_api_spec_content_value
    }
}

resource "azurerm_api_management_product" "product" {
    product_id              = "${var.prefix}-product"
    resource_group_name     = azurerm_api_management.apim_service.resource_group_name
    api_management_name     = azurerm_api_management.apim_service.name
    display_name            = "${var.prefix}-product"
    subscription_required   = true
    approval_required       = false
    published               = true
    description             = "An example Product"
}

resource "azurerm_api_management_api_policy" "api_policy" {
    api_name                = "mcd-project-management-fct"
    api_management_name     = azurerm_api_management_api.api.name
    resource_group_name     = azurerm_api_management.apim_service.resource_group_name

    xml_content = <<XML
    <policies>
    <inbound>
        <base/>
        <set-backend-service id="apim-generated-policy" backend-id="mcd-project-management-fct" />
    </inbound>
    </policies>
    XML
}

// NOTE:
//      Consumption pricing tier does not support management for API group & Product group
//      so temporarily commented out
// TODO:
//      Depending on pricing tier, apply the API group & Product group

/*
resource "azurerm_api_management_group" "group" {
    name                    = "${var.prefix}-group"
    resource_group_name     = var.group_name
    api_management_name     = azurerm_api_management.apim_service.name
    display_name            = "${var.prefix}-group"
    description             = "An example group"
}

resource "azurerm_api_management_product_api" "product_api" {
  resource_group_name       = var.group_name
  api_management_name       = azurerm_api_management.apim_service.name
  product_id                = azurerm_api_management_product.product.product_id
  api_name                  = azurerm_api_management_api.api.name
}

resource "azurerm_api_management_product_group" "product_group" {
  resource_group_name       = var.group_name
  api_management_name       = azurerm_api_management.apim_service.name
  product_id                = azurerm_api_management_product.product.product_id
  group_name                = azurerm_api_management_group.group.name
}
*/