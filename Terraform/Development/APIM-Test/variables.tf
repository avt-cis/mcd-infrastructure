variable "prefix" {
    description     = "(Required) The prefix which should be used for all resources in this script"
    type            = string
}

variable "subscription_id" {
    description     = "(Required) The subscription ID"
    type            = string
}

variable "group_name" {
    description     = "(Required) The name of resource group"
    type            = string
}

variable "location" {
    description     = "(Required) The Azure Region in which all resources in this example should be created"
    type            = string
    default         = "eastus"
}

variable "publisher_name" {
    description     = "(Required) The name of the API service publisher"
    type            = string
    default         = "Alex Choi"
}

variable "publisher_email" {
    description     = "(Required) The email of the API service publisher"
    type            = string
    default         = "alex.choi@cognex.com"
}

variable "sku_name" {
    description     = "(Required) The name of SKU"
    type            = string
}

variable "open_api_spec_content_format" {
    description     = "The format of the content from which the API Definition should be imported. Possible values are: openapi, openapi+json, openapi+json-link, openapi-link, swagger-json, swagger-link-json, wadl-link-json, wadl-xml, wsdl and wsdl-link."
    type            = string
    default         = "swagger-link-json"
}

variable "open_api_spec_content_value" {
    description     = "The Content from which the API Definition should be imported. When a content_format of *-link-* is specified this must be a URL, otherwise this must be defined inline."
    type            = string
    default         = "http://conferenceapi.azurewebsites.net/?format=json"
}

variable "apim_policy" {
    description     = "(Optional) The policy of the API processing in the form of XML."
}