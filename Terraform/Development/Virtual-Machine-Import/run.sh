export subscription_id=bffb5be7-cc13-43b6-90d9-38964ec0b45b

az account set --subscription $subscription_id

terraform init
terraform import azurerm_virtual_machine.example /subscriptions/bffb5be7-cc13-43b6-90d9-38964ec0b45b/resourceGroups/azure-traffic-manager-introduction/providers/Microsoft.Compute/virtualMachines/vm-eastus2