terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.92.0"
    }
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "example" {
  name     = "alex-rg-examplemdl"
}

resource "azurerm_api_management" "example" {
  name                = "example-apim-alex-test"
  location            = data.azurerm_resource_group.example.location
  resource_group_name = data.azurerm_resource_group.example.name
  publisher_name      = "cognex"
  publisher_email     = "company@terraform.io"

  sku_name = "Consumption_0"
}

resource "azurerm_api_management_backend" "example" {
  name                = "example-backend"
  resource_group_name = data.azurerm_resource_group.example.name
  api_management_name = azurerm_api_management.example.name
  protocol            = "http"
  url                 = "https://my-alex-function.azurewebsites.net"
}