export subscription_id=f62f3254-4b22-45cc-bb0f-34d135f9b33e
export resource_group_name=rg-lz-corp-modular-deeplearning-production
export apim_name=modular-dl-production-apim-service

az account set --subscription $subscription_id

terraform init
terraform import azurerm_api_management.example /subscriptions/$subscription_id/resourceGroups/$resource_group_name/providers/Microsoft.ApiManagement/service/$apim_name