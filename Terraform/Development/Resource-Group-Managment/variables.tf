# Configure the Microsoft Azure Provider
variable "subscription_id" {
    description     = "(Required) The subscription ID"
    type            = string
}

variable "group_name" {
    description     = "The name of resource group"
    type            = string
}

variable "location" {
    description     = "The Azure Region in which all resources in this example should be created"
    type            = string
}

variable "bash_path" {
    description     = "(Required) The path to bash"
    type            = string
}

variable "script_path" {
    description     = "(Required) The path to script.sh"
    type            = string
}