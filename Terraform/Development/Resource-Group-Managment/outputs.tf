output "resource_group_name" {
    description     = "The name of resource group created."
    value           = azurerm_resource_group.rg[0].name
}