terraform {
    required_providers {
        azurerm = {
            source  = "hashicorp/azurerm"
            version = "=2.46.0"
        }
    }
}

provider "azurerm" {
    features {}

    subscription_id   = var.subscription_id
}

data "external" "script" {
    program                 = [var.bash_path, var.script_path]

    query = {
        group_name          = var.group_name
    }
}

resource "azurerm_resource_group" "rg" {
    count                   = data.external.script.result.exists == "true" ? 0 : 1
    name                    = var.group_name
    location                = var.location
}