resource "random_uuid" "rg" {
}

locals {
    resource_group_name = format(
        "%s_rg_%s",
        var.group_name_prefix,
        random_uuid.rg.result
    )
}