terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.92.0"
    }
  }

  backend "azurerm" {
    resource_group_name  = "alex-test-rg"
    storage_account_name = "alexteststorageacc2"
    container_name       = "terraform"
    key                  = "dev.terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}