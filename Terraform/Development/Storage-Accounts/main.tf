data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

resource "azurerm_storage_account" "storageAcc1" {
  resource_group_name               = var.resource_group_name
  name                              = local.storage_acc_name
  location                          = data.azurerm_resource_group.rg.location
  access_tier                       = "Hot"
  account_kind                      = "StorageV2"
  account_replication_type          = "RAGRS"
  account_tier                      = var.account_tier
  allow_blob_public_access          = true
  enable_https_traffic_only         = true
  infrastructure_encryption_enabled = false
  is_hns_enabled                    = false
  min_tls_version                   = "TLS1_2"
  nfsv3_enabled                     = false
  queue_encryption_key_type         = "Service"
  shared_access_key_enabled         = true
  table_encryption_key_type         = "Service"
  tags                              = var.tags

  blob_properties {
    change_feed_enabled      = false
    last_access_time_enabled = false
    versioning_enabled       = false

    container_delete_retention_policy {
      days = 7
    }

    delete_retention_policy {
      days = 7
    }
  }

  network_rules {
    bypass = [
      "AzureServices",
    ]
    default_action             = "Allow"
    ip_rules                   = []
    virtual_network_subnet_ids = []
  }

  queue_properties {
    hour_metrics {
      enabled               = true
      include_apis          = true
      retention_policy_days = 7
      version               = "1.0"
    }

    logging {
      delete                = false
      read                  = false
      retention_policy_days = 7
      version               = "1.0"
      write                 = false
    }

    minute_metrics {
      enabled               = false
      include_apis          = false
      retention_policy_days = 7
      version               = "1.0"
    }
  }

  share_properties {
    retention_policy {
      days = 7
    }
  }

  static_website {
    error_404_document = "index.html"
    index_document     = "index.html"
  }

  timeouts {}
}