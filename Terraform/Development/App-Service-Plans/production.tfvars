resource_group_name = "alex-test-rg-1"
app_service_plan_name_prefix = "frontend"
app_service_plan_kind = "Linux"
app_service_plan_sku  = {
  tier = "Standard"
  size = "S1"
}