provider "azurerm" {
    features {}

    subscription_id               = var.subscription_id
}

resource "azurerm_api_management_authorization_server" "auth_server" {
    display_name                  = var.display_name
    name                          = var.server_id
    description                   = var.server_description
    api_management_name           = var.apim_name
    resource_group_name           = var.resource_group_name
    client_registration_endpoint  = var.client_registration_endpoint
    grant_types                   = var.grant_types
    authorization_endpoint        = "https://login.microsoftonline.com/${var.tenant_id}/oauth2/v2.0/authorize"
    token_endpoint                = "https://login.microsoftonline.com/${var.tenant_id}/oauth2/v2.0/token"
    authorization_methods         = ["GET", "POST"]
    client_id                     = var.client_id
    client_authentication_method  = var.client_authentication_method
    bearer_token_sending_methods  = var.bearer_token_sending_methods
    default_scope                 = var.default_scope
    client_secret                 = var.client_secret
}

/*
    TODO: Connect OAuth2 server to API (in Sercurity setting)
*/