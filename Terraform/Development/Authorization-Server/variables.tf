variable "subscription_id" {
    description     = "(Required) The subscription ID"
    type            = string
}

variable "display_name" {
    description     = "(Required) The user-friendly name of this Authorization Server."
    type            = string
    default         = "auth.test.server"
}

variable "server_id" {
    description     = "(Required) The ID of this Authorization Server."
    type            = string
    default         = "auth-test-server"
}

variable "server_description" {
    description     = "(Optional) A description of the Authorization Server, which may contain HTML formatting tags."
    type            = string
    default         = "This is a test authorization server."
}

variable "apim_name" {
    description     = "(Required) The name of the API Management Service in which this Authorization Server should be created. Changing this forces a new resource to be created."
    type            = string
    default         = ""
}

variable "resource_group_name" {
    description     = "(Required) The name of the Resource Group in which the API Management Service exists. Changing this forces a new resource to be created."
    type            = string
    default         = ""
}

variable "client_registration_endpoint" {
    description     = "(Required) The URI of page where Client/App Registration is performed for this Authorization Server"
    type            = string
    default         = "https://localhost"
}

variable "grant_types" {
    type            = list
    description     = "(Required) Form of Authorization Grants required when requesting an Access Token. Possible values are authorizationCode, clientCredentials, implicit and resourceOwnerPassword."
    default         = ["authorizationCode",]
}

variable "tenant_id" {
    type            = string
    description     = "(Required) The tenant ID of the client app [organizations / common / tenant_ID]"
    default         = "organizations"
}

variable "client_id" {
    type            = string
    description     = "(Required) The Client/App ID registered with this Authorization Server."
    default         = ""
}

variable "client_authentication_method" {
    type            = list
    description     = "(Optional) The Authentication Methods supported by the Token endpoint of this Authorization Server. Possible values are Basic and Body."
    default         = ["Body"]
}

variable "bearer_token_sending_methods" {
    type            = list
    description     = "(Optional) The mechanism by which Access Tokens are passed to the API. Possible values are authorizationHeader and query."
    default         = ["authorizationHeader"]
}

variable "default_scope" {
    description     = "(Optional) The Default Scope used when requesting an Access Token, specified as a string containing space-delimited values."
    type            = string
    default         = ""
}

variable "client_secret" {
    description     = "(Optional) The Client/App Secret registered with this Authorization Server."
    type            = string
    default         = ""
}