export subscription_id=9a8e1a59-ab84-49af-a717-7f2582116098
export resource_group_name=finetuning-dev_rg_4al2
export cluster_name=Finetuning-AKS-Cluster
export pool_name=gpupoolwoody

az account set --subscription 9a8e1a59-ab84-49af-a717-7f2582116098

terraform init
terraform import azurerm_kubernetes_cluster_node_pool.pool1 /subscriptions/$subscription_id/resourcegroups/$resource_group_name/providers/Microsoft.ContainerService/managedClusters/$cluster_name/agentPools/$pool_name