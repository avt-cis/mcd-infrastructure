resource_group_name = "terraform_test_rg_c745792c-bbbe-6f5e-d84a-5cee797d34cc"
app_service_plan_name_prefix = "frontend"
app_service_plan_kind = "Linux"
app_service_plan_sku  = {
  tier = "Standard"
  size = "S1"
}
