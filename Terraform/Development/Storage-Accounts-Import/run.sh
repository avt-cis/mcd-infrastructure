export subscription_id=9a8e1a59-ab84-49af-a717-7f2582116098
export resource_group_name=fine-tuning-dev_rg_ccff3406-efaf-2170-79b0-84b5f54c9f93
export storage_acc_name=finetuningdev

az account set --subscription $subscription_id

terraform init
terraform import azurerm_storage_account.example /subscriptions/$subscription_id/resourceGroups/$resource_group_name/providers/Microsoft.Storage/storageAccounts/$storage_acc_name