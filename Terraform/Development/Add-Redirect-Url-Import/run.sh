set -e
set -x

export ARM_SUBSCRIPTION_ID="9a8e1a59-ab84-49af-a717-7f2582116098"

az login -u "alex.choi@cognex.com" -p "@#CLOUDdeveloper32"
az account set --subscription $ARM_SUBSCRIPTION_ID

### import variable
export TF_VAR_object_id="52585548-3913-4421-bf0b-bb9e77447e6a"

### delete '.tfstate' file
count=`find . -name "*.tfstate*" -type f  | wc -l`
if [ $count != 0 ]
then 
    find . -name "*.tfstate*" -type f -delete
fi

### operate terraform script
terraform init
terraform import azuread_application.ad $TF_VAR_object_id