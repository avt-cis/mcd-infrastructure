set -e
set -x

terraform init
terraform validate
terraform plan -var-file 'production.tfvars'
terraform apply -var-file 'production.tfvars' -auto-approve