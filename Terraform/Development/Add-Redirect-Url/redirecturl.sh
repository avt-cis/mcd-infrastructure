#!/bin/sh

# Add Redirect url in existed AD's app
set -e
set -x

az login --service-principal -u b0c91cdc-a41a-41f8-8c61-08ab0d4c9ddb -p o527Q~Gs6z_TEjL.Nvgx2LBM8k2QdKl~~Y2qb --tenant c12007a4-882b-4381-b05a-b783431570c7

az account set --subscription=$1
clientid=$(az ad app create --display-name $2 --query appId --output tsv)
objectid=$(az ad app show --id $clientid --query objectId --output tsv)
redirecttype=spa
graphurl=https://graph.microsoft.com/v1.0/applications/$objectid
az rest --method PATCH --uri $graphurl --headers 'Content-Type=application/json' --body '{"'$redirecttype'":{"redirectUris":["'$3'"]}}'