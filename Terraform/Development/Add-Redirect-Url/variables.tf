variable "subscription_id" {
  description = "(required) Azure subscription ID"
  type        = string
  default     = "bffb5be7-cc13-43b6-90d9-38964ec0b45b"
}

variable "reg_app_name" {
  description = "(required) Name of the app registraion in Active Directory"
  type        = string
  default     = "mdl-deployment"
}

variable "redirect_url" {
  description = "(required) Redirect URL address"
  type        = string
  default     = "https://modulardlfrontendstatic.z22.web.core.windows.net/projects"
}