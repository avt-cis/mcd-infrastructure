locals {
    command_line = format(
        "bash redirecturl.sh %s %s %s",
        var.subscription_id,
        var.reg_app_name,
        var.redirect_url
    )
}
