#!/bin/bash
set -x
set -e

export ARM_SUBSCRIPTION_ID="9a8e1a59-ab84-49af-a717-7f2582116098"
export ARM_CLIENT_ID="d107e0b1-ff40-4628-992a-4b9b1662851c"
export ARM_CLIENT_SECRET="mcs7Q~KntigfARk121chbLvTkv57hDymUG4M5"
export ARM_TENANT_ID="c12007a4-882b-4381-b05a-b783431570c7"

#az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID
az login -u "alex.choi@cognex.com" -p "@#CLOUDdeveloper32"
az account set --subscription $ARM_SUBSCRIPTION_ID

# settings
RG_NAME=finetuning-dev_rg_IFSC
RG_LOCATE=eastus
AKS_NAME=Finetuning-AKS-Cluster
AKS_NODE_NUM=2
ACR_NAME=finetuningdevqurh
ACR_URL=finetuningdevqurh.azurecr.io

# # create resource group
# az group create --name $RG_NAME --location $RG_LOCATE

# # create AKS
# az aks create --resource-group $RG_NAME --name $AKS_NAME --node-count $AKS_NODE_NUM \
#     --nodepool-name agentpool \
#     --vm-set-type VirtualMachineScaleSets \
#     --node-vm-size Standard_DS2_v2 \
#     --load-balancer-sku standard \
#     --enable-cluster-autoscaler \
#     --min-count 1 \
#     --max-count 3 \
#     --cluster-autoscaler-profile scan-interval=10s\
#     --enable-addons monitoring --generate-ssh-keys

# # create container registry
# # using existing container registry in this scenario
# az acr create --resource-group $RG_NAME --name $ACR_NAME --sku Standard

# # connect container registry to AKS
az aks update -n $AKS_NAME -g $RG_NAME --attach-acr $ACR_NAME

# # get credential
# az aks get-credentials --resource-group $RG_NAME --name $AKS_NAME --overwrite-existing

# check container registry
az aks check-acr --name $AKS_NAME --resource-group $RG_NAME --acr $ACR_URL