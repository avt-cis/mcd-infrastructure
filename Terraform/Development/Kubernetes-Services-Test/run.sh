set -e
set -x

az account set --subscription bffb5be7-cc13-43b6-90d9-38964ec0b45b

terraform init
terraform validate
terraform plan
terraform apply -auto-approve
#terraform destroy -auto-approve