terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.92.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_api_management" "example" {
}

resource "azurerm_api_management_backend" "example" {
}