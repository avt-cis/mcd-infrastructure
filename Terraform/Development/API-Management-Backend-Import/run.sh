export subscription_id=f62f3254-4b22-45cc-bb0f-34d135f9b33e
export apim_resource_group_name=rg-lz-corp-modular-deeplearning-production
export apim_backend_resource_group_name=demo-1.0
export apim_name=modular-dl-production-apim-service
export backend_name=mcd-project-management-fct-prod

az account set --subscription $subscription_id

terraform init
#terraform import azurerm_api_management.example /subscriptions/$subscription_id/resourceGroups/$apim_resource_group_name/providers/Microsoft.ApiManagement/service/$apim_name
terraform import azurerm_api_management_backend.example /subscriptions/$subscription_id/resourceGroups/$apim_backend_resource_group_name/providers/Microsoft.ApiManagement/service/$apim_name/backends/$backend_name