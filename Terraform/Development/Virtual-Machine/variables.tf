variable "rg_name_prefix" {
  default = "alex-rg"
}

variable "rg_location" {
    default = "koreacentral"
}