set -e
set -x

export ARM_SUBSCRIPTION_ID="bffb5be7-cc13-43b6-90d9-38964ec0b45b"
export USERNAME="alex.choi@cognex.com"
export USERPASSWORD="@#CLOUDdeveloper32"

az login -u $USERNAME -p $USERPASSWORD
az account set --subscription $ARM_SUBSCRIPTION_ID

terraform init
terraform validate
terraform plan -var-file 'production.tfvars'
terraform apply -var-file 'production.tfvars' -auto-approve
# terraform destroy -var-file 'production.tfvars' -auto-approve