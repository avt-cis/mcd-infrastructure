output "resource_group_name" {
  description = "The name of resource group created."
  value       = azurerm_resource_group.rg.name
}

output "resource_group_id" {
  description = "The id of resource group created."
  value       = azurerm_resource_group.rg.id
}
