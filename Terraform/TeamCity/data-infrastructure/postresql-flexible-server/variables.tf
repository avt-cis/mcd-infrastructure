variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type        = string
}

variable "resource_group_location" {
  description = "(required) Location of the target Resource Group to deploy"
  type        = string
}

variable "psql_flex_server_name_prefix" {
  description = "(required) Name prefix of the Azure Database for PostgreSQL flexible servers to deploy / (possible) 3 ~ 63 characters, lowercase letters, numbers, hyphens"
  type        = string
}

variable "psql_admin_name" {
  description = "(required) primary administrator username for the server / (possible) 1 ~ 63 characters and numbers"
  type        = string
}

variable "psql_compute_tier_size" {
  description = "(required) The computer tier and size for the server. the name follow the tier + name pattern. (e.g. B_Standard_B1ms, GP_Standard_D2s_v3, MO_Standard_E4s_v3)."
  type        = string
}

variable "psql_storage_size" {
  description = "(required) The max storage(in MB) allowed for the server."
  type        = number
  validation {
    condition     = contains([32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432], var.psql_storage_size)
    error_message = "Possible values are '32768', '65536', '131072', '262144', '524288', '1048576', '2097152', '4194304', '8388608', '16777216', '33554432'."
  }
}

variable "psql_availability_zone" {
  description = "(required) The availability zone of the server."
  type        = number
    validation {
    condition     = contains([1, 2, 3], var.psql_availability_zone)
    error_message = "Possible values are '1', '2' and '3'."
  }
}

variable "psql_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

variable "development_mode" {
  description = "(optional) If 'development_mode' is true, All IP addresses is allowed access to server"
  type        = bool
  default     = false
}

variable "allow_all_ip" {
  description = "(optional) Configuration for allowing access to all IPs for server."
  type        = object({
    name              = string
    start_ip_address  = string
    end_ip_address    = string
  })
  default = {
    name              = "allow_all_ip"
    start_ip_address  = "0.0.0.0"
    end_ip_address    = "255.255.255.255"
  }
}
