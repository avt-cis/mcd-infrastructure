resource "azurerm_postgresql_flexible_server" "psqlserver" {
  administrator_login           = var.psql_admin_name
  administrator_password        = local.psql_admin_password
  backup_retention_days         = 7
  geo_redundant_backup_enabled  = false
  location                      = var.resource_group_location
  name                          = local.psql_flex_server_name
  resource_group_name           = var.resource_group_name
  sku_name                      = var.psql_compute_tier_size
  storage_mb                    = var.psql_storage_size
  tags                          = var.psql_tags
  version                       = "12"
  zone                          = var.psql_availability_zone

  timeouts {}
}

resource "azurerm_postgresql_flexible_server_firewall_rule" "psqlserver_firewall_rule" {
  count            = var.development_mode ? 1 : 0
  name             = var.allow_all_ip.name
  server_id        = azurerm_postgresql_flexible_server.psqlserver.id
  start_ip_address = var.allow_all_ip.start_ip_address
  end_ip_address   = var.allow_all_ip.end_ip_address
}