resource "random_string" "random" {
  length  = 8
  special = false
}

resource "random_password" "password" {
  length           = 64
  special          = true
}

locals {
  psql_flex_server_name = format(
      "%s%s",
      lower(var.psql_flex_server_name_prefix),
      lower(random_string.random.result)
  )
  psql_admin_password = random_password.password.result
}