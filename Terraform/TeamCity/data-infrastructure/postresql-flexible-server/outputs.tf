output "resource_group_name" {
  value = resource.azurerm_postgresql_flexible_server.psqlserver.resource_group_name
}

output "psql_admin_name" {
  value = resource.azurerm_postgresql_flexible_server.psqlserver.administrator_login
}

output "psql_admin_password" {
  value = resource.azurerm_postgresql_flexible_server.psqlserver.administrator_password
  sensitive = true
}

output "psql_flex_server_name" {
  value = resource.azurerm_postgresql_flexible_server.psqlserver.name
}

output "pqsl_flex_server_location" {
  value = resource.azurerm_postgresql_flexible_server.psqlserver.location
}

output "pqsl_flex_server_compute_tier_size" {
  value = resource.azurerm_postgresql_flexible_server.psqlserver.sku_name
}

output "pqsl_flex_server_storage_size" {
  value = resource.azurerm_postgresql_flexible_server.psqlserver.storage_mb
}