###########################################################
#  Resource Group
###########################################################
module "resource_group" {
  source = "./resource-group/resource-group"

  group_prefix  = var.resource_group_name_prefix
  location      = var.location
}

/*
module "resource_group_role" {
  source = "./resource-group/resource-group-role"
  role_prefix = "resource_group_role" 
  resource_group_id = module.resource_group.resource_group_id
}
*/

/*
resource "azurerm_role_assignment" "role_assignement" {
  scope = module.resource_group.resource_group_id
  role_definition_id = module.resource_group_role.role_definition_resource_id
  principal_id       = "d886a140-9327-468b-b073-979370ab285f"
}
*/

###########################################################
#  Storage Accounts
###########################################################
module "storage_account" {
  source = "./storage-account"

  resource_group_name     = module.resource_group.resource_group_name
  storage_acc_name_prefix = var.storage_acc_name_prefix
  account_tier            = var.storage_acc_tier
}

###########################################################
#  PostgreSQL Flexible Server
###########################################################
module "postgresql_flexible_server" {
  source = "./postresql-flexible-server"

  resource_group_name           = module.resource_group.resource_group_name
  resource_group_location       = var.location
  psql_flex_server_name_prefix  = var.psql_flex_server_name_prefix
  psql_admin_name               = var.psql_admin_name
  psql_compute_tier_size        = var.psql_compute_tier_size
  psql_storage_size             = var.psql_storage_size
  psql_availability_zone        = var.psql_availability_zone
  psql_tags                     = var.psql_tags
}