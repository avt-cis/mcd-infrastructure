###########################################################
#  Common
###########################################################
location = "eastus"

###########################################################
#  Resource Groups
###########################################################
resource_group_name_prefix = "mdl-dev"

###########################################################
#  Storage Accounts
###########################################################
storage_acc_name_prefix  = "mdldev"
storage_acc_tier         = "Standard"
storage_acc_tags         = {
  "project"     = "MDL"
  "envrionment" = "dev"
  "monitoring"  = "true"
}

###########################################################
#  PostgreSQL Flexible Server Configurations
###########################################################
psql_admin_name               = "psqluser"
psql_flex_server_name_prefix  = "mdl-dev"
psql_compute_tier_size        = "B_Standard_B1ms"
psql_storage_size             = 32768
psql_availability_zone        = 2
psql_tags = {
  "envrionment" = "dev"
  "monitoring"  = "true"
}