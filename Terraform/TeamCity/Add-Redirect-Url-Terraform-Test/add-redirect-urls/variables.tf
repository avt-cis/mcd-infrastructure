# variable "display_name" {
#   type = string
# }

variable "object_id" {
  description = "(required) The object id of Azure Active Directory's App"
  type        = string
}

variable "add_redirect_url_platform_type" {
  description = "(required) The plaform type of web applications: 'web' or 'spa' for single page application"
  type        = string
  validation {
    condition     = contains(["web", "spa"], var.add_redirect_url_platform_type)
    error_message = "Argument 'platform_type' must be either of 'web', 'spa'."
  }
  default = "spa"
}

variable "add_redirect_url_redirect_uris" {
  description = "(required) List of redirect URIs to add to app registration"
  type        = list(string)
  default     = []
}