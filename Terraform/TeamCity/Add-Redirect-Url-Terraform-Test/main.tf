###########################################################
#  Add Redirect URLs
###########################################################
module "add_redirect_urls" {
  source = "./add-redirect-urls"

  object_id                       = var.object_id
  add_redirect_url_platform_type  = var.add_redirect_url_platform_type
  add_redirect_url_redirect_uris  = var.add_redirect_url_redirect_uris
}
