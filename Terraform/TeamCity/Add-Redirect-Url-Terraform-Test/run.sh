set -e
set -x

az logout

export ARM_SUBSCRIPTION_ID="9a8e1a59-ab84-49af-a717-7f2582116098"
#export ARM_CLIENT_ID="d107e0b1-ff40-4628-992a-4b9b1662851c"
#export ARM_CLIENT_SECRET="mcs7Q~KntigfARk121chbLvTkv57hDymUG4M5"
#export ARM_TENANT_ID="c12007a4-882b-4381-b05a-b783431570c7"

#az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID
az login -u {YOUR_AZURE_ACCOUNT_NAME} -p {YOUR_AZURE_ACCOUNT_PASSWORD}
az account set --subscription $ARM_SUBSCRIPTION_ID

### import variable
# the object ID of the following app is from https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/Overview/appId/de3ca2fd-13ae-43cc-a258-4a7d1b7504d5/isMSAApp/
export TF_VAR_object_id="2ff79b2a-74a9-4777-93cc-e8f9d236b18f"

### operate terraform script
terraform init
terraform validate
terraform import module.add_redirect_urls.azuread_application.ad $TF_VAR_object_id
terraform plan
terraform apply -auto-approve
# terraform destroy -auto-approve