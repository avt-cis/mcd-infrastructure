###########################################################
#  Add Redirect URLs
###########################################################
add_redirect_url_platform_type  = "spa"
add_redirect_url_redirect_uris  = [
  "https://cideploymenttest.z1.web.core.windows.net/",
  "https://cloudtrainingdemo.z13.web.core.windows.net/",
  "http://localhost:4200/",
  "https://modulardlfrontendstatic.z22.web.core.windows.net/"
]