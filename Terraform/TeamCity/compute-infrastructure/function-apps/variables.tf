variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type = string
}

variable "resource_location" {
  description = "(required) Location of the target resource to deploy"
  type = string
}

variable "function_app_service_plan_kind" {
  description = "(required) Type of the App Service Plan"
  validation {
    condition     = contains(["Linux", "Windows"], var.function_app_service_plan_kind)
    error_message = "Possible values is either 'Linux' or 'Windows'."
  }
}

variable "function_app_service_plan_sku" {
  description = "(required) A sku block as documented below."
  type        = object({
    tier = string
    size = string
  })
  default = {
    tier = "Standard"
    size = "S1"
  }
}

variable "function_app_name" {
  description = "(required) Name of the Function App to deploy"
  type        = string
}

variable "function_app_db_connection_string" {
  description = "(required) Name of the database connection for Function App to deploy"
  type        = string
}

variable "function_app_worker_runtime" {
  description = "(required) Type of worker runtime for the Function App to deploy"
  type        = string
  default     = "python"
  validation {
    condition     = contains(["python", "node"], var.function_app_worker_runtime)
    error_message = "Possible value: 'python', 'node'."
  }
}

variable "function_app_cors_allowed_origins" {
  description = "(optional) List of CORS allowed origins"
  type        = list(string)
  default     = []
}

variable "function_app_storage_acc_tier" {
  description = "(optional)"
  type        = string
  validation {
    condition     = contains(["Standard", "Premium"], var.function_app_storage_acc_tier)
    error_message = "Argument 'account_tier' must either of 'Standard', or 'Premium'."
  }
}