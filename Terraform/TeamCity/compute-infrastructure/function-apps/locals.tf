resource "random_string" "random" {
  length  = 4
  special = false
}

locals {
  app_service_plan_name = format(
      "ASP-%s-%s",
      var.function_app_name,
      random_string.random.result
  )
  storage_acc_name = substr(replace(lower(var.function_app_name), "-", ""), 0, 16)
}