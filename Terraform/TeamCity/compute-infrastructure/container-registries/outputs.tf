output "resource_group_name" {
  value = resource.azurerm_container_registry.acr.resource_group_name
}

output "location" {
  value = resource.azurerm_container_registry.acr.location
}

output "container_reg_name" {
  value = resource.azurerm_container_registry.acr.name
}

output "service_tier" {
  value = resource.azurerm_container_registry.acr.sku
}

output "container_reg_geo_replication" {
  value = [for region in resource.azurerm_container_registry.acr.georeplications : region.location]
}