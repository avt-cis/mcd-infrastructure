output "role_definition_resource_id" {
  description = "id of the role created."
  value       = azurerm_role_definition.role.role_definition_resource_id
}