
resource "azurerm_role_definition" "role" {
  name        = local.role_name
  //scope       = azurerm_resource_group.rg.id
  scope       = var.resource_group_id
  description = "This is a custom role created via Terraform"

  permissions {
    #actions     = ["Microsoft.Resources/subscriptions/resourceGroups/read"]
    actions     = ["*"]
    not_actions = []
  }

  assignable_scopes = [
    var.resource_group_id,
  ]
}
