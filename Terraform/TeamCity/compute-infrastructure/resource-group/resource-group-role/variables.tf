# Configure Azure Services to Create
variable "resource_group_id" {
  description = "(Required) id of the resource group"
  type        = string
}

variable "role_prefix" {
  description = "(Required) prefix for the role name"
  type        = string
}