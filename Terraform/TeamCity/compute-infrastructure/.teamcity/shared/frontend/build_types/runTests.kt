package shared.frontend.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*

import shared.common.build_types.BuildNumberBuild
import shared.common.*
import shared.templates.*
import shared.common.build_steps.*
import shared.frontend.build_steps.*

class RunTests(
    val build_name: String,
    val build_args: String,
    val build_number: BuildNumberBuild,
    val docker_image: DockerImage,
    val agent: Agent,
    val execution_timeout_minutes: Int,
) : BuildType({
    templates(
        ArtifactoryDockerLogin,
        StatusPublisher
    )

    id(sanitize_build_id(build_name))
    name = build_name

    buildNumberPattern = "${build_number.depParamRefs.buildNumber}"

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }

    dependencies {
        snapshot(build_number) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }

    steps {
        npmLoginAllRegistriesLinux()
        restoreAndRunTestsLinux()
    }

    triggers {
        vcs {
            branchFilter = "+:*"
        }
    }

    agent.add_to_requirements(this)

    failureConditions {
        executionTimeoutMin = execution_timeout_minutes
    }

    params {
        param("docker.image", docker_image.name)
        param("docker.image.tag", docker_image.tag)
        param("env.BUILD_ARGS", build_args)
    }
})
