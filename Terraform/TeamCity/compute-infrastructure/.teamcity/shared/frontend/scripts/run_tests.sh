#!/bin/bash

set -e
set -x

echo "##teamcity[progressStart 'Installing project dependencies']"
npm install
echo "##teamcity[progressFinish 'Installing project dependencies']"

echo "##teamcity[progressStart 'Running project-management tests']"
npm run test
echo "##teamcity[progressFinish 'Running project-management tests']"
