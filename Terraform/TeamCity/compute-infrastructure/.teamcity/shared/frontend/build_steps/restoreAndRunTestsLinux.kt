package shared.frontend.build_steps

import shared.common.build_steps.*
import jetbrains.buildServer.configs.kotlin.v2019_2.BuildSteps
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep

fun BuildSteps.restoreAndRunTestsLinux(): BuildSteps {
    val result = BuildSteps()

    result.step(
        script_file(
            "shared/frontend/scripts/run_tests.sh",
            ScriptBuildStep.ImagePlatform.Linux
        )
    )

    return result
}
