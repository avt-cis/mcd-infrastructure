package shared.templates

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildFeatures.commitStatusPublisher

object StatusPublisher : Template({
    name = "status_publisher"

    features {
        commitStatusPublisher {
            vcsRootExtId = "${DslContext.settingsRoot.id}"
            publisher = bitbucketServer {
                url = "%system.git.server.https%"
                userName = "%system.user%"
                password = "%system.password%"
            }
        }
    }
})
