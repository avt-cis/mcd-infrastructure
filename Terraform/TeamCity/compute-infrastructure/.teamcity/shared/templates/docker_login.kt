package shared.templates

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildFeatures.dockerSupport

object ArtifactoryDockerLogin : Template({
    name = "artifactory_docker_login"

    features {
        dockerSupport {
            cleanupPushedImages = true
            loginToRegistry = on {
                dockerRegistryId = "docker_registry_0"
            }
        }
    }
    params {
        param("docker.registry", "%system.artifactory.docker.registry%")
    }
})
