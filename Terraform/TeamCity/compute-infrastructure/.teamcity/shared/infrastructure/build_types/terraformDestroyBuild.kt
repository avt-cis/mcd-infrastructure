package shared.infrastructure.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.*

import shared.common.Agent
import shared.common.Architecture
import shared.common.DockerImage
import shared.common.build_steps.script_step
import shared.common.build_types.BuildNumberBuild
import shared.infrastructure.build_steps.terraformConfigBuildStepLinux
import shared.infrastructure.build_steps.terraformDestroyBuildStepLinux
import shared.templates.ArtifactoryDockerLogin
import shared.templates.StatusPublisher

/*
 In order to use this build_type, the following parameters needs to be set
 Azure Backend:
# terraform.state.resource.group.name
# terraform.state.storage.account.name
# terraform.state.storage.container.name
# terraform.state.state.key
#
# Azure Service Principal environment variables:
# ARM_TENANT_ID
# ARM_SUBSCRIPTION_ID
# ARM_CLIENT_ID
 */

class TerraformDestroyBuild(
    val docker_image: DockerImage,
    val agent: Agent = Agent(architecture= Architecture.AMD64),
    val build_number: BuildNumberBuild,
    val script_path: String,
    val state_file_prefix: String,
    val execution_timeout_minutes: Int = 15,
) : BuildType({
    templates(
        ArtifactoryDockerLogin,
        StatusPublisher
    )

    name = "Destroy terraform"

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }

    dependencies {
        snapshot(build_number) {}
    }

    buildNumberPattern = "${build_number.depParamRefs.buildNumber}"

    steps {
        terraformConfigBuildStepLinux(script_path)
    }

    steps {
        terraformDestroyBuildStepLinux(script_path)
    }


    agent.add_to_requirements(this)

    failureConditions {
        executionTimeoutMin = execution_timeout_minutes
    }

    params {
        param("docker.image", docker_image.name)
        param("docker.image.tag", docker_image.tag)
        param("terraform.state.key", "$state_file_prefix/%teamcity.build.branch%")
        text(name="confirmed", value = "", label = "Are you sure you want to delete this configuration? ",description="Be careful, this will delete everything permanently, if you accept, please write \"delete\"", display = ParameterDisplay.PROMPT)
    }
})
