#!/bin/bash

# Azure Service Principal environment variables:
# ARM_TENANT_ID
# ARM_SUBSCRIPTION_ID
# ARM_CLIENT_ID
# ARM_CLIENT_SECRET

set -e
set -x
conf_file=backend.conf

if [ %confirmed% = "delete" ]; then
    echo "deleting"
else
    echo "not deleting"
    exit 1
fi



export ARM_SAS_TOKEN=$(cat sas_token)
terraform init -backend-config $conf_file
terraform plan -destroy
terraform apply -destroy -auto-approve
