#!/bin/bash


#parameters to set:
# Azure Backend:
# terraform.state.resource.group.name
# terraform.state.storage.account.name
# terraform.state.storage.container.name
# terraform.state.state.key
#
# Azure Service Principal environment variables:
# ARM_TENANT_ID
# ARM_SUBSCRIPTION_ID
# ARM_CLIENT_ID
# ARM_CLIENT_SECRET

set -e
set -x
conf_file=backend.conf

touch $conf_file
echo resource_group_name=\"%terraform.state.resource.group.name%\" >> $conf_file
echo storage_account_name=\"%terraform.state.storage.account.name%\" >> $conf_file
echo container_name=\"%terraform.state.storage.container.name%\" >> $conf_file
echo key=\"%terraform.state.key%\" >> $conf_file

az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID

expiry_date=$(date -d "+60 minutes" '+%%Y-%%m-%%dT%%H:%%MZ')
echo $expiry_date

sas_token=$(az storage container generate-sas \
    --account-name  %terraform.state.storage.account.name%\
    --name %terraform.state.storage.container.name% \
    --permissions acdlrw \
    --expiry $expiry_date\
    --auth-mode login \
    --as-user)

echo $(eval echo $sas_token) > sas_token

cat $conf_file
