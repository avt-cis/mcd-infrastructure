package shared.infrastructure.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep

import shared.common.build_steps.script_file

fun BuildSteps.terraformDestroyBuildStepLinux(script_path: String): BuildSteps {
    val result = BuildSteps()

    result.step(
        script_file(
            "$script_path/terraform_destroy.sh",
            ScriptBuildStep.ImagePlatform.Linux
        )
    )


    return result
}