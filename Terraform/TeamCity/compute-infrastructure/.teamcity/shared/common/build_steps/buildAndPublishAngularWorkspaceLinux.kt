package shared.common.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep
import shared.common.*

fun BuildSteps.buildAndPublishAngularWorkspaceLinux(working_directory: String = ""): BuildSteps {
    val result = BuildSteps()

    result.step(
        script_file(
            "shared/common/scripts/build_and_publish_angular_workspace.sh",
            ScriptBuildStep.ImagePlatform.Linux,
            {},
            working_directory
        )
    )

    return result
}
