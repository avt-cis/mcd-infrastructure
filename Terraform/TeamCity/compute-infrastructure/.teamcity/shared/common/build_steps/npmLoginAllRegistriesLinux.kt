package shared.common.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep

fun BuildSteps.npmLoginAllRegistriesLinux(): BuildSteps {
    val result = BuildSteps()

    result.step(
        script_file(
            "shared/common/scripts/npm_login_all_registries.sh",
            ScriptBuildStep.ImagePlatform.Linux
        )
    )


    return result
}
