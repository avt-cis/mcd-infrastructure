package shared.common.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.DslContext
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep
import shared.common.*

/**
 * Helper function to generate a ScriptBuildStep that performs a `git lfs pull`
 * @param submodules, a list of map containing the paths to all submodules for which to git lfs pull
 * @param init, optional ScriptBuildStep init function
 * @returns a ScriptBuildStep
 */
fun BuildSteps.linux_lfs_pull(repository_path: String = "",
                              fetch_include: String = "",
                              fetch_exclude: String = "",
                              init: ScriptBuildStep.() -> Unit = {}) : ScriptBuildStep {
    val result = ScriptBuildStep(init)
    result.name = "" + repository_path.replace("/", "_")
    result.scriptContent = """
        set -e
        set -x

        git lfs install --system

        if [ -n "${repository_path}" ]; then
            cd "${repository_path}"
        fi

        if [ -n "${fetch_include}" ]; then
            git config -f .lfsconfig lfs.fetchinclude "${fetch_include}"
        fi

        if [ -n "${fetch_exclude}" ]; then
            git config -f .lfsconfig lfs.fetchexclude "${fetch_exclude}"
        fi

        git lfs pull
    """.trimIndent()

    result.dockerImage = "%docker.registry%/checkout-gitlfs-alpine-%teamcity.agent.jvm.os.arch%:5ce947ccc14e8b9a3532bdf5ffa416bf79c65410"
    result.dockerImagePlatform = ScriptBuildStep.ImagePlatform.Linux
    result.dockerPull = true
    result.param("org.jfrog.artifactory.selectedDeployableServer.downloadSpecSource", "Job configuration")
    result.param("org.jfrog.artifactory.selectedDeployableServer.useSpecs", "false")
    result.param("org.jfrog.artifactory.selectedDeployableServer.uploadSpecSource", "Job configuration")

    step(result)
    return result
}
