package shared.common.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.DslContext
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep
import java.io.*
import shared.common.*

/**
 * Helper function to read the content of a separate script file
 * @param path, the path to the script file to read, relative to settings.kts
 * @return a string containing the content of the script file
 *
 * source: https://www.jetbrains.com/help/teamcity/kotlin-dsl.html#How+to+Access+Auxiliary+Scripts+from+DSL+Settings
 */
fun readScript(path: String): String {
    val bufferedReader: BufferedReader = File(DslContext.baseDir, path).bufferedReader()
    return bufferedReader.use { it.readText() }.trimIndent()
}

/**
 * Helper function to generate a ScriptBuildStep that reads the content of a script file
 *
 * It assumes the following parameters are defined
 *      %docker.registry%
 *      %docker.image.name%
 *      %docker.image.tag%
 *      %docker.run.parameters%
 * @param script_path, the path to the script to run
 * @param docker_image_platform, the platform on which the docker image is run
 * @param init, optional ScriptBuildStep init function
 * @returns a ScriptBuildStep
 */
fun BuildSteps.script_file(
    script_path: String,
    docker_image_platform: ScriptBuildStep.ImagePlatform,
    init: ScriptBuildStep.() -> Unit = {},
    working_directory: String = ""
): ScriptBuildStep {
    val result = ScriptBuildStep(init)
    result.name = script_path.substringAfterLast("/").substringAfterLast("\\").substringBeforeLast(".")
    result.scriptContent = readScript(script_path)

    result.dockerImage = "%docker.registry%/%docker.image%:%docker.image.tag%"
    result.dockerImagePlatform = docker_image_platform
    result.dockerPull = true
    if (working_directory.length > 0) {
      result.workingDir = working_directory
    }
    result.param("org.jfrog.artifactory.selectedDeployableServer.downloadSpecSource", "Job configuration")
    result.param("org.jfrog.artifactory.selectedDeployableServer.useSpecs", "false")
    result.param("org.jfrog.artifactory.selectedDeployableServer.uploadSpecSource", "Job configuration")

    step(result)
    return result
}

/**
 * Helper function to generate a ScriptBuildStep that reads the content of a script file
 *
 * It assumes the following parameters are defined
 *      %docker.registry%
 * @param script_path, the path to the script to run
 * @param docker_image, an instance of the DockerImage class, containing all infos about the docker
 * image to run the script on
 * @param init, optional ScriptBuildStep init function
 * @returns a ScriptBuildStep
 */
fun BuildSteps.script_file(
    script_path: String,
    docker_image: DockerImage,
    init: ScriptBuildStep.() -> Unit = {}
): ScriptBuildStep {
    val result = ScriptBuildStep(init)
    result.name = script_path.substringAfterLast("/").substringAfterLast("\\").substringBeforeLast(".")
    result.scriptContent = readScript(script_path)

    result.dockerImage = "%docker.registry%/${docker_image.name}:${docker_image.tag}"
    result.dockerImagePlatform = docker_image.Platform
    result.dockerRunParameters = docker_image.run_parameters
    result.dockerPull = true
    result.param("org.jfrog.artifactory.selectedDeployableServer.downloadSpecSource", "Job configuration")
    result.param("org.jfrog.artifactory.selectedDeployableServer.useSpecs", "false")
    result.param("org.jfrog.artifactory.selectedDeployableServer.uploadSpecSource", "Job configuration")

    step(result)
    return result
}

/**
 * Helper function to generate a ScriptBuildStep from a string.
 *
 * It assumes the following parameters are defined
 *      %docker.registry%
 *      %docker.image.name%
 *      %docker.image.tag%
 *      %docker.run.parameters%
 * @param script_name, name of the build step
 * @param script_content, content of the script
 * @param init, optional ScriptBuildStep init function
 * @returns a ScriptBuildStep
 */
fun BuildSteps.script_step(
    step_name: String,
    script_content: String,
    init: ScriptBuildStep.() -> Unit = {}
): ScriptBuildStep {
    val result = ScriptBuildStep(init)
    result.name = step_name
    result.scriptContent = script_content.trimIndent()

    result.dockerImage = "%docker.registry%/%docker.image.name%:%docker.image.tag%"
    result.dockerRunParameters = "%docker.run.parameters%"
    result.dockerPull = true

    result.param("org.jfrog.artifactory.selectedDeployableServer.downloadSpecSource", "Job configuration")
    result.param("org.jfrog.artifactory.selectedDeployableServer.useSpecs", "false")
    result.param("org.jfrog.artifactory.selectedDeployableServer.uploadSpecSource", "Job configuration")

    step(result)
    return result
}
