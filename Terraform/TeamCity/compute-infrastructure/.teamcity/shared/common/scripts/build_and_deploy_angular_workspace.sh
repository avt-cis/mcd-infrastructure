#!/bin/bash

set -e
set -x

WORKING_DIR="%teamcity.build.checkoutDir%"

$WORKING_DIR/.teamcity/shared/scripts/build_angular_workspace.sh

ACCOUNT_NAME=%env.ACCOUNT_NAME%
SAS_TOKEN=%env.SAS_TOKEN%

echo "##teamcity[progressStart 'Deploying using Azure CLI']"
curl -sL https://aka.ms/InstallAzureCLIDeb | bash

az storage blob upload-batch --account-name $ACCOUNT_NAME --sas-token $SAS_TOKEN --source 'dist' --destination '$web'
echo "##teamcity[progressFinish 'Deploying using Azure CLI']"
