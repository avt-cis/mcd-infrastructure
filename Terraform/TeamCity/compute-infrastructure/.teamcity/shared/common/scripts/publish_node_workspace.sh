#!/bin/bash

set -e
set -x

CHECKOUT_ROOT_DIR=$1

echo "##teamcity[progressStart 'Getting version string']"
VERSION=$($CHECKOUT_ROOT_DIR/.teamcity/shared/scripts/get_version_string_from_tags.sh)
echo "##teamcity[progressFinish 'Getting version string']"

echo "##teamcity[progressStart 'Publishing package']"
cd dist && npm version $VERSION --commit-hooks false --git-tag-version false && cd ..
npm publish dist/ --registry %system.npm-public.server.url%/
echo "##teamcity[progressFinish 'Publishing package']"
