#!/bin/bash

set -e
set -x

echo "##teamcity[progressStart 'Restoring dependencies']"
npm install
echo "##teamcity[progressFinish 'Restoring dependencies']"

echo "##teamcity[progressStart 'Building package']"
npm run build
echo "##teamcity[progressFinish 'Building package']"
