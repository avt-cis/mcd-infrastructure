#!/bin/bash

set -e
set -x

LAST_TAG=$(git describe --tags)
if [[ $LAST_TAG =~ ([0-9]+)((\.[0-9]+)+)(.+)* ]];
then
  VERSION="${BASH_REMATCH[1]}${BASH_REMATCH[2]}"
  NUM_DOTS=$(echo $VERSION | tr -cd '.' | wc -c)
  if [[ $NUM_DOTS == 1 ]]; then
    VERSION="${VERSION}.0"
  fi
else
  echo "Result of 'git describe --tags' did not return a parsable version"
  exit 1
fi
VERSION="${VERSION}${BASH_REMATCH[4]}"
echo $VERSION