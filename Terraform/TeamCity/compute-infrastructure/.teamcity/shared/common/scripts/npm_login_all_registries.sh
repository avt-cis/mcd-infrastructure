#!/bin/bash

set -e
set -x

NPM_USER="%system.user%"
NPM_PASSWORD='%system.password%'
NPM_EMAIL="%system.user.email%"
WORKING_DIR="%teamcity.build.checkoutDir%"

apt update
apt install expect-dev -y

echo "##teamcity[progressStart 'Logging ci user into npm registries']"
expect -f $WORKING_DIR/.teamcity/shared/scripts/npm_login.exp "%system.npm-public.server.natick.url%" $NPM_USER $NPM_PASSWORD $NPM_EMAIL
expect -f $WORKING_DIR/.teamcity/shared/scripts/npm_login.exp "%system.npm-public.server.url%" $NPM_USER $NPM_PASSWORD $NPM_EMAIL

npm config get
cat /root/.npmrc >> .npmrc

echo "@cognex:registry=%system.npm-public.server.natick.url%" >> .npmrc
echo "@cognex-mdl:registry=%system.npm-public.server.url%" >> .npmrc

echo "##teamcity[progressFinish 'Logging ci user into npm registries']"
