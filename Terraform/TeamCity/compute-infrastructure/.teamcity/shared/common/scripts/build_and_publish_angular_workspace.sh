#!/bin/bash

set -e
set -x

WORKING_DIR="%teamcity.build.checkoutDir%"

$WORKING_DIR/.teamcity/shared/scripts/build_angular_workspace.sh
$WORKING_DIR/.teamcity/shared/scripts/publish_node_workspace.sh $WORKING_DIR
