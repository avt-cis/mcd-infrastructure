package shared.common

fun sanitize_build_id(build_name: String): String {
    return build_name
        .replace("\\s".toRegex(), "")
        .replace("-", "_")
        .replace(".", "")
        .replace("/", "_")
        .replace("&", "And")
}
