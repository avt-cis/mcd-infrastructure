package shared.common.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.*

import shared.common.*
import shared.templates.*
import shared.common.build_steps.*

class BuildAndDeployAngularWorkspaceBuild(
    val build_name: String,
    val build_number: BuildType,
    val docker_image: DockerImage,
    val agent: Agent,
    val execution_timeout_minutes: Int
) : BuildType({
    templates(
        ArtifactoryDockerLogin,
        StatusPublisher
    )

    id(sanitize_build_id(build_name))
    name = build_name

    buildNumberPattern = "${build_number.depParamRefs.buildNumber}"

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }

    dependencies {
        snapshot(build_number) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }

    steps {
        npmLoginAllRegistriesLinux()
        buildAndDeployAngularWorkspaceLinux()
    }

    agent.add_to_requirements(this)

    failureConditions {
        executionTimeoutMin = execution_timeout_minutes
    }

    params {
        param("docker.image", docker_image.name)
        param("docker.image.tag", docker_image.tag)
        param("env.ACCOUNT_NAME", "")
        param("env.SAS_TOKEN", "")
    }
})
