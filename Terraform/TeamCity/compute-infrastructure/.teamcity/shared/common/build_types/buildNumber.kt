package shared.common.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.*

class BuildNumberBuild() : BuildType({
    id("BuildNumber")
    name = "build number"
    description = "Increments a build number"
    type = BuildTypeSettings.Type.COMPOSITE

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }
    
    requirements {
    }
})

