package shared.common.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import shared.common.*

class TriggerBuild(
    val build_name: String,
    val build_id : String = build_name,
    val builds_list: List<BuildType>,
    val triggerMe: Triggers.() -> Unit = {},
    val notifyMe: BuildFeatures.() -> Unit = {}
) : BuildType({

    name = build_name
    id(sanitize_build_id(build_id))

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }

    dependencies {
        for (build in builds_list) {
            snapshot(build) {}
        }
    }

    requirements {
        // arm is the cheapest and fastest, so that's the preferred arch for trivial builds
        equals("teamcity.agent.jvm.os.arch", "amd64")
    }

    triggers(triggerMe)

    features(notifyMe)
})
