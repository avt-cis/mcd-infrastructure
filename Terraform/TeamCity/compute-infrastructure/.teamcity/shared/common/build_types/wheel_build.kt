package shared.common.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.BuildType
import jetbrains.buildServer.configs.kotlin.v2019_2.DslContext
import jetbrains.buildServer.configs.kotlin.v2019_2.Template
import shared.common.build_steps.*
import shared.common.*
import shared.templates.*

class WheelBuild(
    val build_name: String,
    val build_id: String = build_name,
    val python_command: String = "python",
    val lfs_pull_step: Boolean = false,
    val conan_config_step: Boolean = false,
    val conan_config_tag: String = "",
    val conan_profile_tag: String = "",
    val wheel_build_preamble: String = "",
    val wheel_build_requirements: String = "-r requirements.txt",
    val wheel_build_command_args: String = "",
    val wheel_test_step: Boolean = false,
    val wheel_test_preamble: String = "",
    val wheel_test_requirements: String = "pytest",
    val wheel_test_command: String = "pytest tests",
    val wheel_check_step: Boolean = true,
    val wheel_check_preamble: String = "",
    val wheel_upload_step: Boolean = true,
    val wheel_upload_preamble: String = "",
    val wheel_upload_pypi_server_address: String,
    val docker_image: DockerImage,
    val agent: Agent = Agent(cpu = CPU.HIGH),
    val timeout_minutes: Int = 120,
    val optional_templates : Array<Template> = arrayOf(StatusPublisher),
) : BuildType({

    name = build_name
    id(sanitize_build_id(build_id))

    templates(
        ArtifactoryDockerLogin,
        *optional_templates
    )

    vcs {
        root(DslContext.settingsRootId)
        cleanCheckout = true
    }

    artifactRules = "dist/*.whl"

    val windows = (docker_image.is_windows())
    val script_preamble = if (windows) "@echo on" else "set -e\nset -x"
    val script_conclusion = if (windows) "if %%ERRORLEVEL%% neq 0 exit /b %%ERRORLEVEL%%" else ""
    val pip = "%wheel.python.command% -m pip"

    steps {

        if (lfs_pull_step) {
            if (windows)
                script_step(
                    step_name = "lfs pull",
                    script_content = """
                    ${script_preamble}
                    echo "##teamcity[progressStart 'git LFS pull']"
                    git lfs install --system
                    git lfs pull
                    ${script_conclusion}
                    echo "##teamcity[progressFinish 'git LFS pull']"
                    """.trimIndent(),
                    init = { dockerImagePlatform = docker_image.Platform }
                )
            else
                linux_lfs_pull()
        }

        if (conan_config_step) {
            script_step(
                step_name = "conan config",
                script_content = """
                ${script_preamble}
                echo "##teamcity[progressStart 'conan config']"
                conan config install https://%system.user%:%system.password%@%system.git.server.dns%/scm/el/conan-config.git --verify-ssl=False -a "-b %conan.config.tag% --depth 1"

                conan user -r conan-cognex -p %system.password% %system.user%
                conan user -r conan-public -p %system.password% %system.user%
                conan user -r conan-security -p %system.password% %system.user%

                conan config install https://%system.user%:%system.password%@%system.git.server.dns%/scm/el/conan-profiles.git --verify-ssl=False -a "-b %conan.profile.tag% --depth 1"
                ${script_conclusion}
                echo "##teamcity[progressFinish 'conan config']"
                """.trimIndent(),
                init = { dockerImagePlatform = docker_image.Platform }
            )
        }

        script_step(
            step_name = "build wheel",
            script_content = """
             ${script_preamble}
             ${wheel_build_preamble}
             echo "##teamcity[progressStart 'creating python wheel']"
             ${pip} install --upgrade pip
             ${pip} install %wheel.build.requirements%
             %wheel.python.command% setup.py bdist_wheel %wheel.build.command.args%
             ${script_conclusion}
             echo "##teamcity[progressFinish 'creating python wheel']"
            """.trimIndent(),
            init = { dockerImagePlatform = docker_image.Platform }
        )

        if (wheel_test_step) {
            script_step(
                step_name = "test wheel",
                script_content = """
                ${script_preamble}
                ${wheel_test_preamble}
                echo "##teamcity[progressStart 'testing wheel']"
                ${pip} install --upgrade pip
                ${pip} install %wheel.test.requirements%
                %wheel.test.command%
                ${script_conclusion}
                echo "##teamcity[progressFinish 'testing wheel']"
                """.trimIndent(),
                init = { dockerImagePlatform = docker_image.Platform }
            )
        }
        if (wheel_check_step) {
            script_step(
                step_name = "check wheel",
                script_content = """
                ${script_preamble}
                ${wheel_check_preamble}
                echo "##teamcity[progressStart 'checking python wheel']"
                ${pip} install --upgrade pip
                ${pip} install twine==3.4.2
                %wheel.python.command% -m twine check --strict dist/*.whl
                ${script_conclusion}
                echo "##teamcity[progressFinish 'checking python wheel']"
                """.trimIndent(),
                init = { dockerImagePlatform = docker_image.Platform }
            )
        }
        if (wheel_upload_step) {
            script_step(
                step_name = "upload wheel",
                script_content = """
                ${script_preamble}
                ${wheel_upload_preamble}
                echo "##teamcity[progressStart 'uploading python wheel']"
                ${pip} install --upgrade pip
                ${pip} install twine==3.4.2
                %wheel.python.command% -m twine upload dist/*.whl
                ${script_conclusion}
                echo "##teamcity[progressFinish 'uploading python wheel']"
                """.trimIndent(),
                init = { dockerImagePlatform = docker_image.Platform }
            )
        }
    }

    params {
        param("agent.cpu.count", agent.CPUs)
        param("agent.gpu.count", agent.GPUs)
        param("agent.architecture", agent.Architecture)
        param("docker.image.name", docker_image.name)
        param("docker.image.tag", docker_image.tag)
        param("docker.run.parameters", docker_image.run_parameters)
        param("wheel.python.command", python_command)
        param("wheel.build.requirements", wheel_build_requirements)
        param("wheel.build.command.args", wheel_build_command_args)
        param("wheel.test.requirements", wheel_test_requirements)
        param("wheel.test.command", wheel_test_command)
        param("conan.config.tag", conan_config_tag)
        param("conan.profile.tag", conan_profile_tag)
        param("env.TWINE_REPOSITORY_URL", wheel_upload_pypi_server_address)
        password("env.TWINE_USERNAME", "%system.user%")
        password("env.TWINE_PASSWORD", "%system.password%")
        param("env.CONAN_USER_HOME", "%system.teamcity.build.tempDir%")
    }

    requirements {
        contains("teamcity.agent.jvm.os.arch", "%agent.architecture%")
        equals("teamcity.agent.hardware.cpuCount", "%agent.cpu.count%")
        equals("teamcity.agent.hardware.gpuCount", "%agent.gpu.count%")
    }

    failureConditions {
        executionTimeoutMin = timeout_minutes
    }
})
