package shared.common

import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep.ImagePlatform

enum class DockerOS {
    ANY,
    LINUX,
    WINDOWS,
}

interface IDockerImage {
    val OS: String
    val Platform: ImagePlatform

    fun is_windows(): Boolean {
        return OS.equals(DockerOS.WINDOWS.toString(), ignoreCase=true)
    }
}

class DockerImage(
    val name: String,
    val tag: String,
    val os: DockerOS = DockerOS.LINUX,
    val run_parameters: String = "",
) : IDockerImage {
    override val OS: String = this.os.toString().toLowerCase()
    override val Platform: ImagePlatform
        get() = ScriptBuildStep.ImagePlatform.valueOf(OS.capitalize())
}
