package shared.common

import jetbrains.buildServer.configs.kotlin.v2019_2.Requirements
import jetbrains.buildServer.configs.kotlin.v2019_2.BuildType

enum class Architecture {
    AMD64,
    AARCH64,
}

enum class CPU(val count: UInt) {
    LOW(2u),
    MEDIUM(4u),
    HIGH(8u),
    VERY_HIGH(16u),
    INSANE(64u);

    companion object {
        fun fromUInt(value: UInt) = CPU.values().firstOrNull { it.count == value }
    }
}

enum class GPU(val count: UInt) {
    NONE(0u),
    ONE(1u),
    TWO(2u),
    FOUR(4u),
    EIGHT(8u);

    companion object {
        fun fromUInt(value: UInt) = GPU.values().firstOrNull { it.count == value }
    }
}

interface IAgent {
    val CPUs: String
    val GPUs: String
    val Architecture: String

    fun add_to_requirements(build : BuildType)
}

class Agent(
    val cpu: CPU = CPU.MEDIUM,
    val architecture: Architecture = Architecture.AMD64,
    val gpu: GPU = GPU.NONE,
) : IAgent {

    override val CPUs: String = this.cpu.count.toString()
    override val GPUs: String = this.gpu.count.toString()
    override val Architecture: String = this.architecture.toString().toLowerCase()

    override fun add_to_requirements(build : BuildType) {
        val arch = this.Architecture
        val cpus = this.CPUs
        val gpus = this.GPUs
        build.requirements{
            equals("teamcity.agent.jvm.os.arch", arch)
            equals("teamcity.agent.hardware.cpuCount", cpus)
            equals("teamcity.agent.hardware.gpuCount", gpus)
        }
    }
}


