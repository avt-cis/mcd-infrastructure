# shared

> shared: Not the quaint public square you see in nearly every Mexican town, but
> rather any defined drug marketplace, such as a smuggling point. Much of the
> violence since December 2006, when President Felipe Calderon declared war on
> drug cartels, is due to fighting among gangs over coveted shareds, or turf,
> including street-level sales taking place in tienditas.

[Source](https://www.latimes.com/la-fg-narco-glossary28-2009oct28-story.html)

This repository contains a kotlin package _shared_, implementing common
utilities for TeamCity, as used in Cartel.

## Templates

The `templates` folder contains code for common and reusable TeamCity
templates.

The main advantages of defining these templates here, in a separate git
repository are the following:

* Avoid code duplication in each TeamCity configuration that needs to
  use these functionalities. Pulling in this repository as a submodule allows
  to reference the required template directly.
* Provide versioning for these templates. Defining them in one (top-level)
  TeamCity project would also allow to avoid code duplication but if any of
  these top-level object is modified, that impacts all project referencing it.
  Similarly, if a project wants to reference a new version of that template,
  the ID of that object needs to be changed manually in the configuration.
* Each instanciation of these templates is still local and independent of other
  instantiations.
