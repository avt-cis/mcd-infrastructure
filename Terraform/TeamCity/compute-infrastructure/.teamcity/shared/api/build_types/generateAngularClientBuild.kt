package shared.api.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import shared.common.*
import shared.common.build_steps.*
import shared.templates.*
import shared.api.build_steps.*

class GenerateAngularClientBuild(
    val build_name: String,
    val compilation_build: BuildType,
    val docker_image: DockerImage,
    val agent: Agent,
    val execution_timeout_minutes: Int,
    val package_name: String
) : BuildType({
    templates(
        ArtifactoryDockerLogin,
        StatusPublisher
    )

    name = build_name
    id(sanitize_build_id(build_name))

    buildNumberPattern = "${compilation_build.depParamRefs.buildNumber}"

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }

    dependencies {
        snapshot(compilation_build) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }

    steps {
        npmLoginAllRegistriesLinux()
        generateAngularClientBuildStepLinux()
        buildAndPublishAngularWorkspaceLinux("out/typescript-angular")
    }

    agent.add_to_requirements(this)

    failureConditions {
        executionTimeoutMin = execution_timeout_minutes
    }

    params {
        param("docker.image", docker_image.name)
        param("docker.image.tag", docker_image.tag)
        param("env.PACKAGE_NAME", package_name)
    }
})
