package shared.api.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.*

import shared.api.build_steps.checkFormattingBuildStepLinux
import shared.common.*
import shared.templates.*
import shared.common.build_types.*

class CheckFormattingBuild(
    val docker_image: DockerImage,
    val agent: Agent = Agent(architecture=Architecture.AMD64),
    val build_number: BuildNumberBuild,
    val script_path: String,
    val api_filename: String,
    val execution_timeout_minutes: Int = 15,
) : BuildType({
    templates(
        ArtifactoryDockerLogin,
        StatusPublisher
    )

    name = "Check formatting"

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }

    dependencies {
        snapshot(build_number) {}
    }

    buildNumberPattern = "${build_number.depParamRefs.buildNumber}"

    steps {
        checkFormattingBuildStepLinux(script_path)
    }

    agent.add_to_requirements(this)

    failureConditions {
        executionTimeoutMin = execution_timeout_minutes
    }

    params {
        param("docker.image", docker_image.name)
        param("docker.image.tag", docker_image.tag)
        param("env.API_FILENAME", api_filename)
    }
})
