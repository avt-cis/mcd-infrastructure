package shared.api.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep
import shared.common.*
import shared.common.build_steps.*

fun BuildSteps.documentationBuildStepLinux(script_path: String): BuildSteps {
    val result = BuildSteps()

    result.step(
        script_file(
            "$script_path/version.sh",
            ScriptBuildStep.ImagePlatform.Linux
        )
    )

    result.step(
        script_file(
            "$script_path/redoc.sh",
            ScriptBuildStep.ImagePlatform.Linux
        )
    )


    return result
}
