package shared.api.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep
import shared.common.build_steps.*

fun BuildSteps.generateAngularClientBuildStepLinux(): BuildSteps {
    val result = BuildSteps()

    result.step(linux_lfs_pull())

    result.step(
        script_file(
            "shared/api/scripts/generate_angular_client.sh",
            ScriptBuildStep.ImagePlatform.Linux
        )
    )

    return result
}
