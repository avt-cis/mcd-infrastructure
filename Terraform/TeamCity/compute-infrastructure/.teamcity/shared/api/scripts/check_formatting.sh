set -e
set -x

echo "##teamcity[progressStart 'installing yamllint']"
apt update
apt install yamllint -y
echo "##teamcity[progressFinish 'installing yamllint']"

echo "##teamcity[progressStart 'running yamllint']"
#fails if syntax is not correct
find . -name '*.yml' -exec yamllint --strict -f parsable -d "{extends: default}" {} +
echo "##teamcity[progressFinish 'running yamllint']"

echo "##teamcity[progressStart 'installing swagger-cli']"
npm install -g @apidevtools/swagger-cli
echo "##teamcity[progressFinish 'swagger-cli installation done']"

echo "##teamcity[progressStart 'running bundle']"
swagger-cli bundle -r -t yaml -o bundled.yml $API_FILENAME
echo "##teamcity[progressStart 'running bundle']"

echo "##teamcity[progressStart 'running validate']"
swagger-cli validate bundled.yml
echo "##teamcity[progressStart 'running validate']"
