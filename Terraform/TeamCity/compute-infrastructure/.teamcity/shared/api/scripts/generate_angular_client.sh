#!/bin/bash

set -e
set -x

# rm -rf out/typescript-angular

PACKAGE_NAME=%env.PACKAGE_NAME%
CHECKOUT_DIR=%teamcity.build.checkoutDir%

echo "##teamcity[progressStart 'Installing openapi-generator']"
apt update
apt install openjdk-8-jre -y
npm install @openapitools/openapi-generator-cli -g
openapi-generator-cli version-manager set 5.3.1
echo "##teamcity[progressFinish 'Installing openapi-generator']"

echo "##teamcity[progressStart 'Generating angular client']"
openapi-generator-cli generate \
  -i $PACKAGE_NAME.yml \
  -g typescript-angular \
  -o out/typescript-angular \
  --additional-properties ngVersion=10,npmName=$PACKAGE_NAME,npmVersion=1.0.0,providedIn=root,queryParamObjectFormat=json,withInterfaces=true
echo "##teamcity[progressFinish 'Generating angular client']"

echo "##teamcity[progressStart 'Installing angular']"
npm install -g @angular/cli
npm link @angular/cli
echo "##teamcity[progressFinish 'Installing angular']"

echo "##teamcity[progressStart 'Adding angular.json']"
cp $CHECKOUT_DIR/.teamcity/file_replacements/replacement_angular.json out/typescript-angular/angular.json
echo "##teamcity[progressFinish 'Adding angular.json']"

echo "##teamcity[progressStart 'Restoring dependencies']"
[ -f ".npmrc" ] && cp ".npmrc" "out/typescript-angular/.npmrc"
cd out/typescript-angular
node $CHECKOUT_DIR/.teamcity/shared/api/scripts/rename_package.js
npm uninstall \
  tsickle \
  ng-packagr
npm install
echo "##teamcity[progressFinish 'Restoring dependencies']"

echo "##teamcity[progressStart 'Upgrading Angular 11 dependencies']"
npm install --save-dev \
  @angular-devkit/core@11
echo "##teamcity[progressFinish 'Upgrading Angular 11 dependencies']"

echo "##teamcity[progressStart 'Upgrading to Angular 11']"
npx @angular/cli@11 update @angular/core@11
echo "##teamcity[progressFinish 'Upgrading to Angular 11']"

echo "##teamcity[progressStart 'Upgrading Angular 12 dependencies']"
npm install --save-dev \
  @angular-devkit/core@12
echo "##teamcity[progressFinish 'Upgrading Angular 12 dependencies']"

echo "##teamcity[progressStart 'Upgrading to Angular 12']"
npx @angular/cli@12 update @angular/core@12
echo "##teamcity[progressFinish 'Upgrading to Angular 12']"

echo "##teamcity[progressStart 'Adding latest compiler']"
npm install --save-dev \
  ng-packagr@12 \
  tsickle@~0.43.0
echo "##teamcity[progressFinish 'Adding latest compiler']"
