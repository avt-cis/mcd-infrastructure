#!/usr/bin/env python
import argparse
import yaml

parser = argparse.ArgumentParser()
parser.add_argument("--filename", dest="filename", help="filename")
parser.add_argument("--version", dest="version", help="filename")

args = parser.parse_args()
filename = args.filename
version = args.version
print(version)

file_list = {}
with open(filename) as file:
    file_list = yaml.load(file, Loader=yaml.FullLoader)
    file_list["info"]["version"] = version


with open(filename, "w") as file:
    documents = yaml.dump(file_list, file)
