set -e
set -x

echo "##teamcity[progressStart 'installing swagger-cli']"
npm install @redocly/openapi-cli -g

echo "##teamcity[progressFinish 'swagger-cli installation done']"

echo "##teamcity[progressStart 'running bundle']"
openapi lint  $API_FILENAME --skip-rule no-unused-components --skip-rule no-empty-servers 2>&1 | tee output.txt

if  grep -q "warning\|failed\|Failed" "output.txt" ; then
         exit 1 ; 
fi


openapi lint bundled.yml --skip-rule no-unused-components --skip-rule no-empty-servers 2>&1 | tee output.txt

if  grep -q "warning\|failed\|Failed" "output.txt" ; then
         exit 1 ; 
fi

echo "##teamcity[progressStart 'running bundle']"
