#!/usr/bin/env node

var packagePath = process.cwd() + '/package.json';

var package = require(packagePath);

if (!package) return 0;

for (let [key, value] of Object.entries(package)) {
    if (key === 'name' && !value.startsWith('@cognex-mdl/')) {
        package[key] = `@cognex-mdl/${value}`;
    }
}

var fs = require('fs');
fs.writeFileSync(packagePath, JSON.stringify(package, null, 2), function(err) {
    if (err) {
        console.log(err);
        return 1;
    }
});
return 0;