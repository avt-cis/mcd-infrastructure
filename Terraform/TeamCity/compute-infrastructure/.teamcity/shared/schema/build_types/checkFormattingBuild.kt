package shared.schema.build_types

import jetbrains.buildServer.configs.kotlin.v2019_2.*

import shared.schema.build_steps.checkFormattingBuildStepLinux
import shared.common.*
import shared.templates.*


class CheckFormattingBuild(
    val docker_image: DockerImage,
    val script_path: String,
    val agent: Agent = Agent(architecture=Architecture.AMD64),
    val execution_timeout_minutes: Int = 15,
) : BuildType({
    templates(
        ArtifactoryDockerLogin,
        StatusPublisher
    )

    name = "Check formatting"
    id(sanitize_build_id(name))

    vcs {
        root(DslContext.settingsRoot)
        cleanCheckout = true
    }

    steps {
        checkFormattingBuildStepLinux(script_path)
    }

    agent.add_to_requirements(this)

    failureConditions {
        executionTimeoutMin = execution_timeout_minutes
    }

    params {
        param("docker.image", docker_image.name)
        param("docker.image.tag", docker_image.tag)
    }
})
