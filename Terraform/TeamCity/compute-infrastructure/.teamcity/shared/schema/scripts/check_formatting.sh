set -e
set -x

#This script only checks the yaml linting and black

echo "##teamcity[progressStart 'installing yamllint']"
pip install black yamllint
echo "##teamcity[progressFinish 'installing yamllint']"

echo "##teamcity[progressStart 'running yamllint']"
#fails if syntax is not correct
find . -name '*.yml' -exec yamllint --strict -f parsable -d "{extends: default}" {} +
echo "##teamcity[progressFinish 'running yamllint']"

echo "##teamcity[progressStart 'running black']"
#fails if syntax is not correct
black . --check --exclude .teamcity
echo "##teamcity[progressFinish 'running black']"
