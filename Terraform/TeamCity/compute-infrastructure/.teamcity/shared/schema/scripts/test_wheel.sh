
set -e
set -x

#This script only checks the yaml linting and black

echo "##teamcity[progressStart 'setting interpreters']"
pyenv local 3.7.12 3.8.12 3.9.10 3.10.2 
echo "##teamcity[progressFinish 'setting interpreters']"

echo "##teamcity[progressStart 'installing tox']"

pip install tox
echo "##teamcity[progressFinish 'installing tox']"

echo "##teamcity[progressStart 'running tox']"
# uses tox.ini to discover and run the tests.
tox
echo "##teamcity[progressFinish 'running tox']"
