package shared.schema.build_steps

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.ScriptBuildStep
import shared.common.build_steps.*

fun BuildSteps.testWheelStepLinux(script_path: String): BuildSteps {
    val result = BuildSteps()

    result.step(
        script_file(
            script_path="$script_path/test_wheel.sh",
            docker_image_platform = ScriptBuildStep.ImagePlatform.Linux
        )
    )
    
    return result
}
