import jetbrains.buildServer.configs.kotlin.v2019_2.*
import settings.dockerImageTag
import shared.common.*
import shared.common.build_types.BuildNumberBuild
import shared.infrastructure.build_types.TerraformDeployBuild
import shared.infrastructure.build_types.TerraformDestroyBuild
import shared.templates.ArtifactoryDockerLogin
import shared.templates.StatusPublisher


/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the


    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2020.2"

project {


    // Disable editing of project and build settings from the UI to avoid issues with TeamCity
    params {
        param("teamcity.ui.settings.readOnly", "true")
    }

    var all_builds = ArrayList<BuildType>()
    subProject {
        name="compute builds"
        id(sanitize_build_id("api_builds"))
        template(ArtifactoryDockerLogin)
        template(StatusPublisher)

        var docker_image = DockerImage(name = "terraform-x86_64-ubuntu20.04", tag = dockerImageTag, run_parameters = "")

        var agent  = Agent(
            cpu = CPU.LOW,
            architecture =  Architecture.AMD64)

        val build_number = BuildNumberBuild()
        buildType(build_number)

        var terraformDeployBuild = TerraformDeployBuild( docker_image = docker_image, agent = agent, build_number = build_number, script_path = "./shared/infrastructure/scripts/",state_file_prefix="compute", execution_timeout_minutes = 20)
        buildType(terraformDeployBuild)

        var terraformDestroyBuild = TerraformDestroyBuild( docker_image = docker_image, agent = agent, build_number = build_number, script_path = "./shared/infrastructure/scripts/",state_file_prefix="compute", execution_timeout_minutes = 20)
        buildType(terraformDestroyBuild)

        all_builds.add(terraformDeployBuild)
    }

    params {
        param("env.ARTIFACTORY_URL", "%system.artifactory.registry.url%")
    }

}
