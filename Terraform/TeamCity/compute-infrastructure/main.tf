###########################################################
#  Resource Group
###########################################################
module "resource_group" {
  source = "./resource-group/resource-group"

  group_prefix  = var.resource_group_name_prefix
  location      = var.location
}

/*
module "resource_group_role" {
  source = "./resource-group/resource-group-role"

  role_prefix       = "resource_group_role" 
  resource_group_id = module.resource_group.resource_group_id
}
*/

###########################################################
#  Frontend App Service Deployment
###########################################################
module "frontend_app_service" {
  source = "./app-services"
  depends_on = [module.resource_group]

  resource_group_name       = module.resource_group.resource_group_name
  app_service_plan_location = module.resource_group.resource_group_location
  app_service_plan_kind     = var.app_service_plan_kind
  app_service_plan_sku      = var.app_service_plan_sku
  app_service_name_prefix   = var.app_service_name_prefix
}

###########################################################
#  Container Registries
###########################################################
module "container_registries" {
  source = "./container-registries"
  depends_on = [module.resource_group]

  resource_group_name           = module.resource_group.resource_group_name
  container_reg_location        = module.resource_group.resource_group_location
  container_reg_name_prefix     = var.container_reg_name_prefix
  container_reg_service_tier    = var.container_reg_service_tier
  container_reg_tags            = var.container_reg_tags
  container_reg_geo_replication = var.container_reg_geo_replication
}

###########################################################
#  Batch Accounts
###########################################################
module "batch_accounts" {
  source = "./batch-accounts"
  depends_on = [module.resource_group]

  resource_group_name       = module.resource_group.resource_group_name
  batch_acc_name_prefix     = var.batch_acc_name_prefix
  batch_acc_location        = module.resource_group.resource_group_location
  batch_acc_tags            = var.batch_acc_tags
  batch_pool                = var.batch_pool
}

###########################################################
#  Function Apps
###########################################################
module "function_apps" {
  source = "./function-apps"
  depends_on = [module.resource_group]

  resource_group_name                       = module.resource_group.resource_group_name
  resource_location                         = module.resource_group.resource_group_location
  function_app_service_plan_kind            = var.function_app_service_plan_kind
  function_app_service_plan_sku             = var.function_app_service_plan_sku
  function_app_storage_acc_tier             = var.function_app_storage_acc_tier
  function_app_name                         = "${var.function_app_names[count.index]}"
  function_app_db_connection_string         = var.function_app_db_connection_string
  function_app_worker_runtime               = var.function_app_worker_runtime
  function_app_cors_allowed_origins         = var.function_app_cors_allowed_origins
  count                                     = "${length(var.function_app_names)}"
}

###########################################################
#  Add Redirect URLs
###########################################################
module "add_redirect_urls" {
  source = "./add-redirect-urls"
  depends_on = [
    module.resource_group
  ]

  //display_name                    = data.azuread_application.ad_app.display_name
  object_id                       = var.object_id
  add_redirect_url_platform_type  = var.add_redirect_url_platform_type
  add_redirect_url_redirect_uris  = var.add_redirect_url_redirect_uris
}
