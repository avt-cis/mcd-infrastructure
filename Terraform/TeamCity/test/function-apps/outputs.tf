output "function_app_name" {
  description = "The name of the Function App deployed."
  value       = azurerm_function_app.functionapp.name
}

output "function_app_id" {
  description = "The ID of the Function App deployed."
  value       = azurerm_function_app.functionapp.id
}
