output "resource_group_name" {
  value = resource.azurerm_storage_account.storageAcc1.resource_group_name
}

output "resource_group_location" {
  value = resource.azurerm_storage_account.storageAcc1.location
}

output "storage_acc_name" {
  value = resource.azurerm_storage_account.storageAcc1.name
}

output "storage_acc_account_tier" {
  value = resource.azurerm_storage_account.storageAcc1.account_tier
}

output "storage_acc_account_kind" {
  value = resource.azurerm_storage_account.storageAcc1.account_kind
}