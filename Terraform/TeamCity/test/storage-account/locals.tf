resource "random_string" "random" {
  length  = 4
  special = false
}

locals {
  storage_acc_name = format(
    "%s%s",
    lower(var.storage_acc_name_prefix),
    lower(resource.random_string.random.result)
  )
}