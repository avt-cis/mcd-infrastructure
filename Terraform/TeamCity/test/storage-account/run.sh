set -e
set -x

az account set -s "9a8e1a59-ab84-49af-a717-7f2582116098"

terraform init
terraform validate
terraform plan -var-file 'production.tfvars'
terraform apply -var-file 'production.tfvars' -auto-approve
terraform output -json > output.json
terraform destroy -var-file 'production.tfvars' -auto-approve