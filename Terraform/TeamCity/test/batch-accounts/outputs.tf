output "resource_group_name" {
  value = resource.azurerm_batch_account.batch_acc.resource_group_name
}

output "location" {
  value = resource.azurerm_batch_account.batch_acc.location
}

output "batch_acc_name" {
  value = resource.azurerm_batch_account.batch_acc.name
}

output "batch_pool" {
  value = tomap({
    for k, inst in azurerm_batch_pool.batch_pool : k => {
      pool_name                        = inst.name
      pool_vm_size                     = inst.vm_size
      pool_node_agent_sku_id           = inst.node_agent_sku_id
      pool_operating_system_publisher  = inst.storage_image_reference[*].publisher
      pool_operating_system_offer      = inst.storage_image_reference[*].offer
      pool_operating_system_sku        = inst.storage_image_reference[*].sku
      pool_scale_evaluation_interval   = inst.auto_scale[*].evaluation_interval
      pool_scale_formula               = inst.auto_scale[*].formula
    }
  })
}