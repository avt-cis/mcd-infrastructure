resource "random_uuid" "rg" {
}

locals {
    role_name = format(
        "%s_rg_%s",
        var.role_prefix,
        random_uuid.rg.result
    )
}