###########################################################
#  Common
###########################################################
variable "location" {
  description = "(required) Region to create the resource"
  type        = string
  default     = "eastus"
  validation {
    condition     = contains(["eastus", "eastus2", "westus", "westeurope", "switzerlandnorth", "koreacentral"], var.location)
    error_message = "Argument 'location' must be either of 'eastus', 'eastus2', 'westus' 'westeurope', 'switzerlandnorth', 'koreacentral'."
  }
}

###########################################################
#  Resource Groups
###########################################################
variable "resource_group_name_prefix" {
  description = "(required) Name prefix of resource group name"
  type        = string
}

###########################################################
#  Frontend App Services
###########################################################
variable "app_service_plan_kind" {
  description = "(required) Type of the App Service Plan"
  validation {
    condition     = contains(["Linux", "Windows"], var.app_service_plan_kind)
    error_message = "Possible values is either 'Linux' or 'Windows'."
  }
}

variable "app_service_plan_sku" {
  description = "(required) A sku block as documented below."
  type        = object({
    tier = string
    size = string
  })
}

variable "app_service_name_prefix" {
  description = "(required) Name prefix of the App Service to deploy"
  type        = string
}

###########################################################
#  Container Registries
###########################################################
variable "container_reg_name_prefix" {
  description = "(required) Name Prefix of the Container Registries to deploy / (possible) 3 ~ 24 characters, lowercase letters, numbers"
  type        = string
}

variable "container_reg_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

variable "container_reg_service_tier" {
  description = "(optional) Service Tier to deploy"
  type        = string
  validation {
    condition     = contains(["Basic", "Standard", "Premium"], var.container_reg_service_tier)
    error_message = "Possible values are 'Basic' or 'Standard', or 'Premium'."
  }
}

variable "container_reg_geo_replication" {
  description = "(optional) A set of geo-replication used for this Container Registries"
  type        = list(string)
  default     = []
}

###########################################################
#  Batch Accounts
###########################################################
variable "batch_acc_name_prefix" {
  description = "(required) Name Prefix of the Batch Accounts to deploy / (possible) 3 ~ 24 characters, only allows lowercase letters, numbers"
  type        = string
}

variable "batch_pool" {
  description = "(optional) Batch Pool configurations"
  type        = map(object({
    pool_name = string
    pool_vm_size = string
    pool_node_agent_sku_id = string
    pool_operating_system_publisher = string
    pool_operating_system_offer = string
    pool_operating_system_sku = string
    pool_scale_evaluation_interval = string
    pool_scale_formula = string
  }))
  default     = {}
}

variable "batch_acc_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

###########################################################
#  Function Apps
###########################################################
variable "function_app_service_plan_kind" {
  description = "(required) Type of the App Service Plan"
  validation {
    condition     = contains(["Linux", "Windows"], var.function_app_service_plan_kind)
    error_message = "Possible values is either 'Linux' or 'Windows'."
  }
}

variable "function_app_service_plan_sku" {
  description = "(required) A sku block as documented below."
  type        = object({
    tier = string
    size = string
  })
  default = {
    tier = "Standard"
    size = "S1"
  }
}

variable "function_app_names" {
  description = "(required) Name of the Function App to deploy"
  type        = list(string)
}

variable "function_app_db_connection_string" {
  description = "(required) Name of the database connection for Function App to deploy"
  type        = string
}

variable "function_app_worker_runtime" {
  description = "(required) Type of worker runtime for the Function App to deploy"
  type        = string
  default     = "python"
  validation {
    condition     = contains(["python", "node"], var.function_app_worker_runtime)
    error_message = "Possible value: 'python', 'node'."
  }
}

variable "function_app_cors_allowed_origins" {
  description = "(optional) List of CORS allowed origins"
  type        = list(string)
  default     = []
}

variable "function_app_storage_acc_tier" {
  description = "(optional)"
  type        = string
  validation {
    condition     = contains(["Standard", "Premium"], var.function_app_storage_acc_tier)
    error_message = "Argument 'account_tier' must either of 'Standard', or 'Premium'."
  }
}