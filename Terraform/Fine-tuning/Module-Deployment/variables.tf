###########################################################
#  Common Variables
###########################################################
variable "location" {
  description = "(required) Region to create the resource"
  type        = string
  default     = "eastus"
}

###########################################################
#  Resource Groups Variables
###########################################################
variable "resource_group_name_prefix" {
  description = "(required) Prefix of resource group name"
  type        = string
}

variable "resource_group_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

###########################################################
#  Storage Accounts Variables
###########################################################
variable "storage_acc_name_prefix" {
  description = "(required) Prefix of storage account name"
  type        = string
}

variable "storage_acc_account_tier" {
  description = "(optional) Account Tier of storage account to deploy"
  type        = string
  default     = "Standard"
}

variable "storage_acc_tags" {
  description = "(optional) Some usefule information for storage account"
  type        = map(any)
  default     = {}
}

variable "storage_acc_replication_type" {
  description = "(optional) Replication type of storage account to deploy"
  type        = string
  default     = "LRS"
}

###########################################################
#  Storage Containers Variables
###########################################################
variable "storage_acc_container_names" {
  description = "(required) List of names of containers to deploy"
  type        = list
}

variable "container_access_type" {
  description = "(optional) Access type of the container to add"
  type        = string
}

###########################################################
#  Container Registries Variables
###########################################################
variable "container_reg_name_prefix" {
  description = "(required) Name Prefix of the Container Registries to deploy / (possible) 3 ~ 24 characters, lowercase letters, numbers"
  type        = string
}

variable "container_reg_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

variable "container_reg_service_tier" {
  description = "(optional) Service Tier to deploy"
  type        = string
  validation {
    condition     = contains(["Basic", "Standard", "Premium"], var.container_reg_service_tier)
    error_message = "Possible values are 'Basic' or 'Standard', or 'Premium'."
  }
}

variable "container_reg_geo_replication" {
  description = "(optional) A set of geo-replication used for this Container Registries"
  type        = list(string)
  default     = []
}

###########################################################
#  Kubernetes Clusters Variables
###########################################################
variable "client_id" {
  description = "(required) Client ID of the app registration for service principal"
  type        = string
}

variable "client_secret" {
  description = "(required) Client secret of the app registration for service principal"
  type        = string
}

variable "log_analytics_workspace_name_prefix" {
  description = "(required) Nmae prefix of the log analytics workspace"
  type        = string
  default     = "loganalyticsws"
}

variable "log_analytics_workspace_sku" {
  description = "SKU type of log analytics worskapce"
  type        = string
  default     = "PerGB2018"
}

variable "aks_name" {
  description = "(required) Name of the Kubernetes Cluster"
  type        = string
}

variable "dns_prefix" {
  description = "(required) Name prefix of the Kubernetes Cluster DNS"
  type        = string
}

variable "aks_sku_tier" {
  description = "(optional) SKU Tier"
  type        = string
  default     = "Standard"
}

variable "aks_node_count" {
  description = "(required) Number of AKS nodes"
  type        = number
  default     = 2
}

variable "aks_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}

###########################################################
#  Kubernetes Clusters GPU Node Pool Variables
###########################################################
variable "aks_gpu_node_pool_name" {
  description = "(required) Name of the GPU node pool for Kubernetes Cluster"
  type        = string
  default     = "gpupool"
}

variable "aks_gpu_node_pool_vm_size" {
  description = "(optional) Size of virtual machine for GPU node pool"
  type        = string
  default     = "Standard_NC4as_T4_v3"
}

variable "aks_gpu_node_pool_node_count" {
  description = "(optional) Number of GPU nodes"
  type        = number
  default     = 1
}

variable "aks_gpu_node_pool_min_count" {
  description = "(optional) Minimum number of GPU nodes"
  type        = number
  default     = 1
}

variable "aks_gpu_node_pool_max_count" {
  description = "(optional) Maximum number of GPU nodes"
  type        = number
  default     = 3
}

###########################################################
#  Attaching ACR Variables
###########################################################
variable "subscription_id" {
  description = "(optional) Subscription ID"
  type        = string
  default     = "9a8e1a59-ab84-49af-a717-7f2582116098"
}