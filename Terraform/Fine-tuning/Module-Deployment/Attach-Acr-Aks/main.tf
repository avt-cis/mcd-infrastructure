resource "azurerm_role_assignment" "attach_acr_aks" {
  principal_id                     = var.principal_id
  role_definition_name             = "AcrPull"
  scope                            = var.scope
  skip_service_principal_aad_check = true
}