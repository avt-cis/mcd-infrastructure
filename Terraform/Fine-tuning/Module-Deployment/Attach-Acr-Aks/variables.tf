variable "principal_id" {
  description = "(optional) The Object ID of the user-defined Managed Identity assigned to the Kubelets"
  type        = string
}

variable "scope" {
  description = "(required) The Object ID of the Azure Container Registry"
  type        = string
}