variable "aks_gpu_node_pool_name" {
  description = "(required) Name of the GPU node pool for Kubernetes Cluster"
  type        = string
  default     = "gpupool"
}

variable "kubernetes_cluster_id" {
  description = "(required) ID of the Kubernetes Cluster"
  type        = string
}

variable "aks_gpu_node_pool_vm_size" {
  description = "(optional) Size of virtual machine for GPU node pool"
  type        = string
  default     = "Standard_NC6s_v3"
}

variable "aks_gpu_node_pool_node_count" {
  description = "(optional) Number of GPU nodes"
  type        = number
  default     = 1
}

variable "aks_gpu_node_pool_min_count" {
  description = "(optional) Minimum number of GPU nodes"
  type        = number
  default     = 1
}

variable "aks_gpu_node_pool_max_count" {
  description = "(optional) Maximum number of GPU nodes"
  type        = number
  default     = 3
}