resource "azurerm_kubernetes_cluster_node_pool" "pool" {
    name                   = var.aks_gpu_node_pool_name
    kubernetes_cluster_id  = var.kubernetes_cluster_id
    vm_size                = var.aks_gpu_node_pool_vm_size
    node_count             = var.aks_gpu_node_pool_node_count
    min_count              = var.aks_gpu_node_pool_min_count
    max_count              = var.aks_gpu_node_pool_max_count
    availability_zones     = []
    enable_auto_scaling    = true
    enable_host_encryption = false
    enable_node_public_ip  = false
    fips_enabled           = false
    kubelet_disk_type      = "OS"
    max_pods               = 110
    mode                   = "User"
    node_labels            = {}
    node_taints            = [
        "sku=gpu:NoSchedule",
    ]
    orchestrator_version   = "1.21.9"
    os_disk_size_gb        = 128
    os_disk_type           = "Ephemeral"
    os_sku                 = "Ubuntu"
    os_type                = "Linux"
    priority               = "Regular"
    scale_down_mode        = "Delete"
    spot_max_price         = -1
    tags                   = {}
    ultra_ssd_enabled      = false
    workload_runtime       = "OCIContainer"

    timeouts {}
}