# Configure Azure Services to Create
variable "resource_group_name_prefix" {
  description = "(required) Name prefix of the resource group to deploy"
  type        = string
}

variable "resource_group_location" {
  description = "(required) Azure Region to create the resource"
  type        = string
}

variable "resource_group_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}