###########################################################
#  Resource Groups Deployment
###########################################################
module "resource_group" {
  source = "./Resource-Groups"

  resource_group_name_prefix  = var.resource_group_name_prefix
  resource_group_location     = var.location
  resource_group_tags         = var.resource_group_tags
}

###########################################################
#  Storage Accounts Deployment
###########################################################
module "storage_accounts" {
  source = "./Storage-Accounts"

  resource_group_name           = module.resource_group.resource_group_name
  storage_acc_name_prefix       = var.storage_acc_name_prefix
  storage_acc_location          = module.resource_group.resource_group_location
  storage_acc_account_tier      = var.storage_acc_account_tier
  storage_acc_replication_type  = var.storage_acc_replication_type
  storage_acc_tags              = var.storage_acc_tags
}

###########################################################
#  Storage Containers Deployment
###########################################################
module "storage_containers" {
  source = "./Storage-Containers"

  storage_account_name    = module.storage_accounts.storage_acc_name
  storage_containers_name = "${var.storage_acc_container_names[count.index]}"
  container_access_type   = var.container_access_type
  count                   = "${length(var.storage_acc_container_names)}"
}

###########################################################
#  Container Registries Deployment
###########################################################
module "container_registries" {
  source = "./Container-Registries"

  resource_group_name           = module.resource_group.resource_group_name
  container_reg_location        = module.resource_group.resource_group_location
  container_reg_name_prefix     = var.container_reg_name_prefix
  container_reg_service_tier    = var.container_reg_service_tier
  container_reg_geo_replication = var.container_reg_geo_replication
  container_reg_tags            = var.container_reg_tags
}

###########################################################
#  Kubernetes Clusters Deployment
###########################################################
module "kubernetes_clusters" {
  source = "./Kubernetes-Clusters"

  resource_group_name                 = module.resource_group.resource_group_name
  client_id                           = var.client_id
  client_secret                       = var.client_secret
  log_analytics_workspace_name_prefix = var.log_analytics_workspace_name_prefix
  log_analytics_workspace_sku         = var.log_analytics_workspace_sku
  aks_location                        = module.resource_group.resource_group_location
  aks_name                            = var.aks_name
  dns_prefix                          = var.dns_prefix
  aks_sku_tier                        = var.aks_sku_tier
  aks_node_count                      = var.aks_node_count
  aks_role_assignment_scope           = module.container_registries.container_reg_acr_id
  aks_tags                            = var.aks_tags
}

###########################################################
#  Kubernetes Clusters Node Pool Deployment
###########################################################
module "kubernetes_cluster_node_pool" {
  source = "./Kubernetes-Cluster-Node-Pool"

  aks_gpu_node_pool_name        = var.aks_gpu_node_pool_name
  kubernetes_cluster_id         = module.kubernetes_clusters.kubernetes_cluster_id
  aks_gpu_node_pool_vm_size     = var.aks_gpu_node_pool_vm_size
  aks_gpu_node_pool_node_count  = var.aks_gpu_node_pool_node_count
  aks_gpu_node_pool_min_count   = var.aks_gpu_node_pool_min_count
  aks_gpu_node_pool_max_count   = var.aks_gpu_node_pool_max_count
}

###########################################################
#  Attaching ACR Deployment
###########################################################
module "attach_acr_aks" {
  depends_on = [
    module.kubernetes_clusters.azurerm_kubernetes_cluster,
    module.container_registries.azurerm_container_registry
  ]
  source = "./Attach-Acr-Aks"

  principal_id                     = module.kubernetes_clusters.kubernetes_principal_id
  scope                            = module.container_registries.container_reg_acr_id
}


/*
module "attach_acr_aks" {
  depends_on = [
    module.kubernetes_clusters.azurerm_kubernetes_cluster,
    module.storage_containers.azurerm_storage_account
  ]
  source = "./Attach-Acr-Aks-Backup"

  subscription_id      = var.subscription_id
  resource_group_name  = module.resource_group.resource_group_name
  aks_name             = var.aks_name
  acr_name             = module.container_registries.container_reg_name
  acr_url              = module.container_registries.container_reg_login_server
}
*/