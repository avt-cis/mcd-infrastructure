resource "azurerm_storage_container" "storageAcc" {
  name                  = var.storage_containers_name
  storage_account_name  = var.storage_account_name
  container_access_type = var.container_access_type
}