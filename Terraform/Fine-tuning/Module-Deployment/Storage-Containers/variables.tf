variable "storage_account_name" {
  description = "(required) Name of the target storage account to add container"
  type        = string
}

variable "storage_containers_name" {
  description = "(required) Name of the containers to add"
  type        = string
}

variable "container_access_type" {
  description  = "(optional) Access type of the container to add"
  type         = string
  default      = "private"
}