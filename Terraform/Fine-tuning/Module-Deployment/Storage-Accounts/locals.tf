resource "random_string" "random" {
  length  = 4
  special = false
}

locals {
  storage_acc_name = format(
    "%s%s",
    substr(replace(lower(var.storage_acc_name_prefix), "-", ""), 0, 15),
    lower(resource.random_string.random.result)
  )
}