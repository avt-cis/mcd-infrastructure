variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type        = string
}

variable "storage_acc_name_prefix" {
  description = "(required) Name of the Storage Account to deploy"
  type        = string
}

variable "storage_acc_location" {
  description = "(required) Location of Storage Account deployment"
  type        = string
  default     = "eastus"
}

variable "storage_acc_account_tier" {
  description = "(optional) Account Tier of storage account to deploy"
  type        = string
  default     = "Standard"
  validation {
    condition     = contains(["Standard", "Premium"], var.storage_acc_account_tier)
    error_message = "Argument 'account_tier' must either of 'Standard', or 'Premium'."
  }
}

variable "storage_acc_replication_type" {
  description = "(optional) Replication type of storage account to deploy"
  type        = string
  default     = "LRS"
}

variable "storage_acc_tags" {
  description = "(optional) Additional information for resource management"
  type        = map(any)
  default     = {}
}