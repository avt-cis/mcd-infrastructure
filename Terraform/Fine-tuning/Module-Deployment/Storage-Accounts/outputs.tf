output "storage_acc_name" {
  value = resource.azurerm_storage_account.storageAcc.name
}

output "storage_account_location" {
  value = resource.azurerm_storage_account.storageAcc.location
}

output "storage_acc_account_tier" {
  value = resource.azurerm_storage_account.storageAcc.account_tier
}

output "storage_acc_account_kind" {
  value = resource.azurerm_storage_account.storageAcc.account_kind
}

/*
output "storage_acc_primary_access_key" {
  value = resource.azurerm_storage_account.storageAcc.primary_access_key
}

output "storage_acc_primary_connection_string" {
  value = resource.azurerm_storage_account.storageAcc.primary_connection_string
}

output "storage_acc_primary_blob_connection_string" {
  value = resource.azurerm_storage_account.storageAcc.primary_blob_connection_string
}

output "storage_acc_primary_blob_endpoint" {
  value = resource.azurerm_storage_account.storageAcc.primary_blob_endpoint
}
*/