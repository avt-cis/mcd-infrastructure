###########################################################
#  Common Configurations
###########################################################
location = "eastus"

###########################################################
#  Resource Groups Configurations
###########################################################
resource_group_name_prefix = "finetuning-dev"
resource_group_tags = {
  "project"     = "Fine-tuning"
  "envrionment" = "dev"
  "monitoring"  = "true"
}

###########################################################
#  Storage Accounts Configurations
###########################################################
storage_acc_name_prefix       = "finetuningdev"
storage_acc_account_tier      = "Standard"
storage_acc_replication_type  = "RAGRS"
storage_acc_tags = {
  "project"     = "Fine-tuning"
  "envrionment" = "dev"
  "monitoring"  = "true"
}

###########################################################
#  Storage Containers Configurations
###########################################################
storage_acc_container_names = ["trained-model", "status", "training-data"]
container_access_type       = "private"

###########################################################
#  Container Registries Configurations
###########################################################
container_reg_name_prefix   = "finetuningdev"
container_reg_service_tier  = "Standard"
container_reg_tags          = {
    "project"     = "Fine-tuning"
    "envrionment" = "dev"
    "monitoring"  = "true"
}
container_reg_geo_replication = ["eastus2", "centralus"]

###########################################################
#  Kubernetes Clusters Configurations
###########################################################
client_id                           = "d107e0b1-ff40-4628-992a-4b9b1662851c"
client_secret                       = "mcs7Q~KntigfARk121chbLvTkv57hDymUG4M5"
log_analytics_workspace_name_prefix = "loganalyticsws"
log_analytics_workspace_sku         = "PerGB2018"

aks_name        = "Finetuning-AKS-Cluster"
dns_prefix      = "Finetuning-AKS-Cluster"
aks_sku_tier    = "Free"
aks_node_count  = 2
aks_tags        = {
  "project"     = "Fine-tuning"
  "envrionment" = "dev"
  "monitoring"  = "true"
}

###########################################################
#  Kubernetes Clusters GPU Node Pool Configurations
###########################################################
aks_gpu_node_pool_name        = "gpupool"
aks_gpu_node_pool_vm_size     = "Standard_NC6s_v3"
aks_gpu_node_pool_node_count  = 1
aks_gpu_node_pool_min_count   = 0
aks_gpu_node_pool_max_count   = 3

###########################################################
#  Attaching ACR Configurations
###########################################################
subscription_id     = "9a8e1a59-ab84-49af-a717-7f2582116098"
//acr_name            = "finetuningdevhzrc"
//acr_url             = "finetuningdevhzrc.azurecr.io"