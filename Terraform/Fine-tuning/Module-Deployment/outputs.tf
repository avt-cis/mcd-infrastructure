###########################################################
#  Resource Group Outputs
###########################################################
output "resource_group_name" {
  description = "The name of resource group created."
  value       = module.resource_group.resource_group_name
}

output "resource_group_location" {
  description = "The location of resource group created."
  value       = module.resource_group.resource_group_location
}

output "resource_group_tags" {
  description = "The tags of resource group created."
  value       = module.resource_group.resource_group_tags
}

###########################################################
#  Storage Account Outputs
###########################################################
output "storage_acc_name" {
  value = module.storage_accounts.storage_acc_name
}

output "storage_account_location" {
  value = module.storage_accounts.storage_account_location
}

output "storage_acc_account_tier" {
  value = module.storage_accounts.storage_acc_account_tier
}

output "storage_acc_account_kind" {
  value = module.storage_accounts.storage_acc_account_kind
}

/*
output "storage_acc_primary_access_key" {
  value = module.storage_accounts.primary_access_key
}

output "storage_acc_primary_connection_string" {
  value = module.storage_accounts.primary_connection_string
}

output "storage_acc_primary_blob_connection_string" {
  value = module.storage_accounts.primary_blob_connection_string
}

output "storage_acc_primary_blob_endpoint" {
  value = module.storage_accounts.primary_blob_endpoint
}
*/

###########################################################
#  Container Registries Outputs
###########################################################
output "container_reg_name" {
  value = module.container_registries.container_reg_name
}

output "container_reg_location" {
  value = module.container_registries.location
}

output "container_reg_service_tier" {
  value = module.container_registries.service_tier
}

output "container_reg_geo_replication" {
  value = module.container_registries.container_reg_geo_replication
}
