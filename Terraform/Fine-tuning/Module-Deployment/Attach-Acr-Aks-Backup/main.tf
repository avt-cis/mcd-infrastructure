resource "null_resource" "attach_acr" {
  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = local.command_line
  }
}