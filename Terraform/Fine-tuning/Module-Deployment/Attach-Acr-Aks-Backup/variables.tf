variable "subscription_id" {
  description = "(optional) Subscription ID"
  type        = string
  default     = "9a8e1a59-ab84-49af-a717-7f2582116098"
}

variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type        = string
}

variable "aks_name" {
  description = "(required) Name of the Azure Kubernetes Cluster"
  type        = string
}

variable "acr_name" {
  description = "(required) Name of the Container Registriy to attach"
  type        = string   
}

variable "acr_url" {
  description = "(required) URL of the Container Registriy to attach"
  type        = string
}

variable "user_name" {
  description = "(optional) User name of Azure account"
  type        = string
  default     = "alex.choi@cognex.com"
}

variable "user_password" {
  description = "(optional) User password of Azure account"
  type        = string
  default     = "@#CLOUDdeveloper32"
}