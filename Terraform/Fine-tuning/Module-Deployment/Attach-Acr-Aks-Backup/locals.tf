locals {
    command_line = format(
        "bash ./Attach-Acr-Aks/main.sh %s %s %s %s %s %s %s",
        var.subscription_id,
        var.user_name,
        var.user_password,  
        var.resource_group_name,
        var.aks_name,
        var.acr_name,
        var.acr_url
    )
}
