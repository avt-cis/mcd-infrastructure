#!/bin/sh
set -x
set -e

# login
ARM_SUBSCRIPTION_ID=$1
USER_NAME=$2
USER_PASSWORD=$3
az logout
az login -u $2 -p $3
az account set --subscription $ARM_SUBSCRIPTION_ID

# settings
RG_NAME=$4
AKS_NAME=$5
ACR_NAME=$6
ACR_URL=$7

# connect container registry to AKS
az aks update -n $AKS_NAME -g $RG_NAME --attach-acr $ACR_NAME

# check container registry
az aks check-acr --name $AKS_NAME --resource-group $RG_NAME --acr $ACR_URL

# logout
az logout