variable "client_id" {
  description = "(required) Client ID of the app registration for service principal"
  type        = string
}

variable "client_secret" {
  description = "(required) Client secret of the app registration for service principal"
  type        = string
}

variable "resource_group_name" {
  description = "(required) Name of the target Resource Group"
  type        = string
}

variable "log_analytics_workspace_name_prefix" {
  description = "(required) Nmae prefix of the log analytics workspace"
  type        = string
  default     = "loganalyticsws"
}

variable "log_analytics_workspace_sku" {
  description = "SKU type of log analytics worskapce"
  type        = string
  default     = "PerGB2018"
}

variable "aks_location" {
  description = "(required) Region to create the Kubernetes Cluster"
  type        = string
  default     = "eastus"
}

variable "aks_name" {
  description = "(required) Name of the Kubernetes Cluster"
  type        = string
}

variable "dns_prefix" {
  description = "(required) Name prefix of the Kubernetes Cluster DNS"
  type        = string
}

variable "aks_sku_tier" {
  description = "(optional) SKU Tier"
  type        = string
  default     = "Standard"
}

variable "aks_node_count" {
  description = "(required) Number of AKS nodes"
  type        = number
  default     = 2
}

variable "aks_role_assignment_scope" {
  description = "(required) Scope of role assignment of container registry"
  type        = string
}

variable "aks_tags" {
  description = "(optional) Some useful information"
  type        = map(any)
  default     = {}
}