output "kubernetes_cluster_id" {
  value = azurerm_kubernetes_cluster.cluster1.id
}

output "kubernetes_principal_id" {
  value = azurerm_kubernetes_cluster.cluster1.kubelet_identity[0].object_id
}