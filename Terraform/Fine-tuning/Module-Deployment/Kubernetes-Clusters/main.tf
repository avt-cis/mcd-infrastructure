resource "azurerm_log_analytics_workspace" "log" {
    name                 = local.log_analytics_workspace_name
    location             = var.aks_location
    resource_group_name  = var.resource_group_name
    sku                  = var.log_analytics_workspace_sku
}

resource "azurerm_log_analytics_solution" "log" {
    solution_name          = "ContainerInsights"
    location               = var.aks_location
    resource_group_name    = var.resource_group_name
    workspace_resource_id  = azurerm_log_analytics_workspace.log.id
    workspace_name         = azurerm_log_analytics_workspace.log.name

    plan {
        publisher  = "Microsoft"
        product    = "OMSGallery/ContainerInsights"
    }
}

resource "azurerm_kubernetes_cluster" "cluster1" {
    name                 = var.aks_name
    location             = var.aks_location
    resource_group_name  = var.resource_group_name
    dns_prefix           = var.dns_prefix

    kubernetes_version             = "1.21.9"
    local_account_disabled         = false
    public_network_access_enabled  = true
    sku_tier                       = var.aks_sku_tier

    auto_scaler_profile {
        balance_similar_node_groups       = false
        empty_bulk_delete_max             = "10"
        expander                          = "random"
        max_graceful_termination_sec      = "600"
        max_node_provisioning_time        = "15m"
        max_unready_nodes                 = 3
        max_unready_percentage            = 45
        new_pod_scale_up_delay            = "0s"
        scale_down_delay_after_add        = "10m"
        scale_down_delay_after_delete     = "10s"
        scale_down_delay_after_failure    = "3m"
        scale_down_unneeded               = "10m"
        scale_down_unready                = "20m"
        scale_down_utilization_threshold  = "0.5"
        scan_interval                     = "10s"
        skip_nodes_with_local_storage     = false
        skip_nodes_with_system_pods       = true
    }

    default_node_pool {
        name                          = "agentpool"
        node_count                    = var.aks_node_count
        vm_size                       = "Standard_D2_v2"
        max_count                     = 3
        max_pods                      = 110
        min_count                     = 1
        availability_zones            = []
        enable_auto_scaling           = true
        enable_host_encryption         = false
        enable_node_public_ip         = false
        fips_enabled                  = false
        kubelet_disk_type             = "OS"
        node_labels                   = {}
        node_taints                   = []
        only_critical_addons_enabled  = false
        orchestrator_version          = "1.21.9"
        os_disk_size_gb               = 128
        os_disk_type                  = "Managed"
        os_sku                        = "Ubuntu"
        tags                          = {}
        type                          = "VirtualMachineScaleSets"
        ultra_ssd_enabled             = false
    }

    identity {
        type = "SystemAssigned"
    }

    addon_profile {
      oms_agent {
        enabled                     = true
        log_analytics_workspace_id  = azurerm_log_analytics_workspace.log.id
      }

      azure_policy {
        enabled = false
      }
    }

    network_profile {
        load_balancer_sku  = "Standard"
        network_plugin     = "kubenet"
    }

    role_based_access_control {
        enabled  = true
    }

    tags = var.aks_tags

    timeouts {}
}

/*
resource "azurerm_role_assignment" "acrpull_role" {
  scope                            = var.aks_role_assignment_scope
  role_definition_name             = "AcrPull"
  skip_service_principal_aad_check = true
  principal_id                     = azurerm_kubernetes_cluster.cluster1.kubelet_identity[0].object_id
}
*/

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
provider "azuread" {
  version = "~>2.19.1"
}

data "azuread_service_principal" "aks_principal" {
  //application_id = var.aks_service_principal_client_id
  application_id = "d107e0b1-ff40-4628-992a-4b9b1662851c"
}



data "azurerm_subscription" "primary" {
}

data "azurerm_client_config" "example" {
}

resource "azurerm_role_definition" "example" {
  role_definition_id = "00000000-0000-0000-0000-000000000000"
  name               = "my-custom-role-definition"
  scope              = data.azurerm_subscription.primary.id

  permissions {
    actions     = ["Microsoft.Authorization/roleAssignments/write"]
    not_actions = []
  }

  assignable_scopes = [
    var.aks_role_assignment_scope,
  ]
}

resource "azurerm_role_assignment" "example" {
  name               = "00000000-0000-0000-0000-000000000000"
  scope              = var.aks_role_assignment_scope
  role_definition_id = azurerm_role_definition.example.role_definition_resource_id
  principal_id       = azurerm_kubernetes_cluster.cluster1.kubelet_identity[0].object_id
  skip_service_principal_aad_check = true
}

/*
resource "azurerm_role_assignment" "acrpull_role" {
  scope                            = var.aks_role_assignment_scope
  role_definition_name             = "AcrPull"
  principal_id                     = data.azuread_service_principal.aks_principal.id
  skip_service_principal_aad_check = true
}

resource "azurerm_role_assignment" "example" {
  principal_id                     = azurerm_kubernetes_cluster.example.kubelet_identity[0].object_id
  role_definition_name             = "AcrPull"
  scope                            = azurerm_container_registry.example.id
  skip_service_principal_aad_check = true
}
*/