resource "random_id" "log_analytics_workspace_name_suffix" {
  byte_length = 8
}

locals {
  log_analytics_workspace_name = format(
    "%s-%s",
    var.log_analytics_workspace_name_prefix,
    random_id.log_analytics_workspace_name_suffix.dec
  )
}