# -- Variables -- #
DEV_RESOURCEGROUPNAME='arm-vscode-dev'
PROD_RESOURCEGROUPNAME='arm-vscode-prod'

# destory resources
az group delete --name $DEV_RESOURCEGROUPNAME -y
az group delete --name $PROD_RESOURCEGROUPNAME -y