# -- Variables -- #
SUBSCRIPTIONID='9a8e1a59-ab84-49af-a717-7f2582116098'
DEV_RESOURCEGROUPNAME='arm-vscode-dev'
PROD_RESOURCEGROUPNAME='arm-vscode-prod'
LOCATION='eastus'
TEMPLATEFILENAME='azuredeploy.json'
DEV_PARAMETERSFILENAME='azuredeploy.parameters.dev.json'
PROD_PARAMETERSFILENAME='azuredeploy.parameters.prod.json'

# Azure login
bash login.sh
echo && az account set --subscription $SUBSCRIPTIONID

# development environment
az group create \
    --name $DEV_RESOURCEGROUPNAME \
    --location $LOCATION
az deployment group create \
    --name devenvironment \
    --resource-group $DEV_RESOURCEGROUPNAME \
    --template-file $TEMPLATEFILENAME \
    --parameters $DEV_PARAMETERSFILENAME


# production environment
az group create \
    --name $PROD_RESOURCEGROUPNAME \
    --location $LOCATION
az deployment group create \
    --name prodenvironment \
    --resource-group $PROD_RESOURCEGROUPNAME \
    --template-file $TEMPLATEFILENAME \
    --parameters $PROD_PARAMETERSFILENAME