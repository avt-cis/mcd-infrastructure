# -- Variables -- #
SUBSCRIPTIONID='9a8e1a59-ab84-49af-a717-7f2582116098'
LOCATION='eastus'

# Azure login
bash login.sh
echo && az account set --subscription $SUBSCRIPTIONID

echo "Enter a project name that is used to generate resource names:" &&
read projectName &&

resourceGroupName="${projectName}rg"

az group create \
    --name $resourceGroupName \
    --location $LOCATION

storageAccountName="${projectName}store"
containerName="templates"

echo -n 'Resorce Group Name: ' &&
echo -n $resourceGroupName