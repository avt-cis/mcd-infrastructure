SUBSCRIPTIONID="9a8e1a59-ab84-49af-a717-7f2582116098"
RESOURCEGROUPNAME="arm-vs-test-2"
LOCATION="eastus"
TEMPLATEFILENAME="azuredeploy.json"

echo && az account set --subscription $SUBSCRIPTIONID

# Create a new resource group to contain the template spec.
az group create \
  --name $RESOURCEGROUPNAME \
  --location $LOCATION