# -- Variables -- #
SUBSCRIPTIONID='9a8e1a59-ab84-49af-a717-7f2582116098'
RESOURCEGROUPNAME='arm-vscode-test'
LOCATION='eastus'
TEMPLATEFILENAME='azuredeploy.json'
PARAMETERSFILENAME='azuredeploy.parameters.json'

# Azure login
bash login.sh
echo && az account set --subscription $SUBSCRIPTIONID
az group create --name $RESOURCEGROUPNAME --location $LOCATION
az deployment group create \
    --name addtags \
    --resource-group $RESOURCEGROUPNAME \
    --template-file $TEMPLATEFILENAME \
    --parameters storagePrefix=store storageSKU=Standard_LRS webAppName=demoapp

