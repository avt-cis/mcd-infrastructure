KEYVAULTNAME='ExampleVaultAlexTest2'
KEYVAULTPASSWORD='examplepassword'
KEYVAULTVALUE='hVFkk965BuUv'

az keyvault secret show \
    --name $KEYVAULTPASSWORD \
    --vault-name $KEYVAULTNAME \
    --query $KEYVAULTVALUE