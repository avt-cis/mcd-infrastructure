# -- Variables -- #
SUBSCRIPTIONID='9a8e1a59-ab84-49af-a717-7f2582116098'
LOCATION='eastus'
RESOURCEGROUPNAME='arm-vscode-test'
KEYVAULTNAME='ExampleVaultAlexTest2'
KEYVAULTPASSWORD='examplepassword'
KEYVAULTVALUE='hVFkk965BuUv'

echo && az account set --subscription $SUBSCRIPTIONID

az group create \
    --name $RESOURCEGROUPNAME \
    --location $LOCATION

az keyvault create \
    --name $KEYVAULTNAME \
    --resource-group $RESOURCEGROUPNAME \
    --location $LOCATION \
    --enabled-for-template-deployment true

az keyvault secret set \
    --vault-name $KEYVAULTNAME \
    --name $KEYVAULTPASSWORD \
    --value $KEYVAULTVALUE