# -- Variables -- #
USERNAME='alex.choi@cognex.com'
PASSWORD=''
SUBSCRIPTIONID='9a8e1a59-ab84-49af-a717-7f2582116098'
RESOURCEGROUPNAME='arm-vscode-test'
LOCATION='eastus'
TEMPLATEFILENAME='azuredeploy.json'
PARAMETERSFILENAME='azuredeploy.parameters.json'

read -sp "Azure password: " AZ_PASS && echo && az login -u $username -p $AZ_PASS
#echo && az login -u $USERNAME -p $PASSWORD
echo && az account set --subscription $SUBSCRIPTIONID
az group create --name $RESOURCEGROUPNAME --location $LOCATION
az deployment group create \
    --resource-group $RESOURCEGROUPNAME \
    --template-file $TEMPLATEFILENAME \
    --parameters $PARAMETERSFILENAME

#az group delete --name $RESOURCEGROUPNAME